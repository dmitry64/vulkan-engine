# Vulkan engine


# Build
* Clone repo with submodules: ```git clone ...```
* Install dependencies: ```glm, glm-dev, vulkan, vulkan-dev, glfw3, glfw3-dev```
* Run cmake: ```cmake ./ ```
* Compile: ```make -j9 ```
* Run: ```./engine```
