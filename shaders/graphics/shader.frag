#version 450
#extension GL_ARB_separate_shader_objects : enable
layout(binding = 2) uniform sampler2D texSampler;
layout(binding = 3) uniform sampler2D normalSampler;
layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 fragPos;
layout(location = 3) in vec3 fragNormal;

layout(location = 0) out vec4 outFragColor;

void main() {
	vec3 lightPos = vec3(8.0f,8.0f,8.0f);
	vec3 lightColor = vec3(0.9f,0.9f,0.9f);
	vec4 objectColor = texture(texSampler, fragTexCoord);
    vec3 normal = texture(normalSampler, fragTexCoord).rgb;
    normal = normalize(normal * 2.0 - 1.0);
    vec3 norm = normalize(fragNormal);
	vec3 lightDir = normalize(lightPos - fragPos);  
    float diff = max(dot(normal, lightDir), 0.0);
	vec3 diffuse = objectColor.xyz * lightColor;
	vec3 ambient = objectColor.xyz * 0.1;
    
    vec3 resColor = (ambient + diffuse) * objectColor.xyz;
    outFragColor = vec4(diffuse, 1.0);

}
