#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

layout(binding = 1) uniform ObjectTransformations {
    mat4 model;
} obj;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec2 fragTexCoord;
layout(location = 2) out vec3 fragPos;
layout(location = 3) out vec3 fragNormal;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    mat4 mvpo = ubo.proj * ubo.view * ubo.model * obj.model;
    vec4 pos = mvpo * vec4(inPosition, 1.0);

    gl_Position = pos;
    fragColor = inColor;
    fragTexCoord = inTexCoord;
    fragPos = pos.xyz;
    fragNormal = inNormal;
}
