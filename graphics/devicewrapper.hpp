#ifndef DEVICEWRAPPER_HPP
#define DEVICEWRAPPER_HPP

#include <mutex>
#include <vulkan/vulkan.hpp>

class DeviceWrapper {
    vk::Device _device;
    std::mutex _mutex;

public:
    DeviceWrapper() {}
    inline void lock()
    {
        _mutex.lock();
    }
    inline void unlock()
    {
        _mutex.unlock();
    }
    vk::Device& getDevice()
    {
        return _device;
    }
};

#endif // DEVICEWRAPPER_HPP
