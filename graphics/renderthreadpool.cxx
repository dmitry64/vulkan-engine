#include "renderthreadpool.hpp"
#include <iostream>

RenderThreadPool::RenderThreadPool()
{
}

RenderThreadPool::~RenderThreadPool()
{
}

void RenderThreadPool::render(const vk::RenderPassBeginInfo& renderPassInfo)
{
    const std::vector<DrawableObject>& drawables = _entities;
    std::vector<vk::CommandBuffer> commandBuffers(drawables.size());

    vk::CommandBufferAllocateInfo secondaryAllocInfo;
    secondaryAllocInfo.commandPool = _renderCommandPool;
    secondaryAllocInfo.level = vk::CommandBufferLevel::eSecondary;
    secondaryAllocInfo.commandBufferCount = commandBuffers.size();

    _device->lock();
    if (_device->getDevice().allocateCommandBuffers(&secondaryAllocInfo, commandBuffers.data()) != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate command buffers!" << std::endl;
        std::abort();
    }
    _device->unlock();

    vk::CommandBuffer rootBuffer;
    vk::CommandBufferAllocateInfo primaryAllocInfo;
    primaryAllocInfo.commandPool = _renderCommandPool;
    primaryAllocInfo.level = vk::CommandBufferLevel::ePrimary;
    primaryAllocInfo.commandBufferCount = 1;
    _device->lock();
    if (_device->getDevice().allocateCommandBuffers(&primaryAllocInfo, &rootBuffer) != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate command buffers!" << std::endl;
        std::abort();
    }
    _device->unlock();
    vk::CommandBufferInheritanceInfo inheritanceInfo(renderPassInfo.renderPass, 0, nullptr, 0, vk::QueryControlFlags(), vk::QueryPipelineStatisticFlags());

    for (size_t i = 0; i < commandBuffers.size(); ++i) {
        auto& commandBuffer = commandBuffers[i];
        const auto& drawable = drawables[i];
        vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse | vk::CommandBufferUsageFlagBits::eRenderPassContinue, &inheritanceInfo);

        if (commandBuffer.begin(&beginInfo) == vk::Result::eSuccess) {
            commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, _graphicsPipeline);
            drawable.recordCommandBuffer(commandBuffer, _pipelineLayout);
            commandBuffer.end();
        } else {
            std::cerr << "Command buffers bind fail!" << std::endl;
            std::abort();
        }
    }

    vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

    if (rootBuffer.begin(&beginInfo) == vk::Result::eSuccess) {
        rootBuffer.beginRenderPass(&renderPassInfo, vk::SubpassContents::eSecondaryCommandBuffers);
        rootBuffer.executeCommands(commandBuffers.size(), commandBuffers.data());
        rootBuffer.nextSubpass(vk::SubpassContents::eInline);
        rootBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, _uiPipeline);

        rootBuffer.endRenderPass();
        rootBuffer.end();
    } else {
        std::cerr << "Command buffers bind fail!" << std::endl;
        std::abort();
    }

    std::vector<vk::CommandBuffer> temp;
    temp.push_back(rootBuffer);
    _commandBuffers = temp;
    _bufferReady = true;
}

void RenderThreadPool::doRender(const vk::RenderPassBeginInfo& renderPassInfo)
{
    render(renderPassInfo);
}

void RenderThreadPool::cleanup()
{
    if (_bufferReady) {
        _bufferReady = false;
        _device->lock();
        _device->getDevice().freeCommandBuffers(_renderCommandPool, _commandBuffers.size(), _commandBuffers.data());
        _device->unlock();
    }
}

void RenderThreadPool::doCleanup()
{
    cleanup();
}

void RenderThreadPool::init(std::shared_ptr<DeviceWrapper> device, vk::Pipeline& graphicsPipeline, vk::Pipeline& uiPipeline, vk::PipelineLayout& layout, vk::RenderPass& renderPass, uint32_t queueIndex)
{
    _device = device;
    _graphicsPipeline = graphicsPipeline;
    _uiPipeline = uiPipeline;
    _pipelineLayout = layout;
    _renderPass = renderPass;
    vk::CommandPoolCreateInfo poolInfo;
    poolInfo.queueFamilyIndex = queueIndex;
    _device->lock();
    vk::Result res = _device->getDevice().createCommandPool(&poolInfo, nullptr, &_renderCommandPool);
    _device->unlock();
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to create command pool! error:" << res << std::endl;
        std::abort();
    }
}

void RenderThreadPool::setRenderingData(const std::vector<DrawableObject>& entities)
{
    _entities = entities;
}

std::vector<vk::CommandBuffer> RenderThreadPool::getResults()
{
    return _commandBuffers;
}

void RenderThreadPool::destroyHandlers()
{
    cleanup();
    _device->lock();
    _device->getDevice().destroyCommandPool(_renderCommandPool);
    _device->unlock();
}

void RenderThreadPool::waitForFinished()
{
}
