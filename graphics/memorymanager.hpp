#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include "devicewrapper.hpp"
#include <iostream>
#include <vulkan/vulkan.hpp>

class MemoryManager {
    bool _isInited;
    std::shared_ptr<DeviceWrapper> _device;
    vk::PhysicalDevice _physicalDevice;
    vk::Queue _graphicsQueue;
    vk::CommandPool _memoryCommandPool;
    vk::DescriptorPool _memoryDescriptorPool;
    vk::DescriptorSetLayout _descriptorSetLayout;

public:
    MemoryManager();
    void init(std::shared_ptr<DeviceWrapper> device, vk::PhysicalDevice& physicalDevice, vk::Queue& graphicsQueue, vk::CommandPool& memoryCommandPool, vk::DescriptorPool& memoryDescriptorPool, vk::DescriptorSetLayout& descriptorSetLayout);
    void createBuffer(size_t size, const vk::BufferUsageFlags& usage, const vk::MemoryPropertyFlags& properties, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory) const;
    uint32_t findMemoryType(uint32_t typeFilter, const vk::MemoryPropertyFlags& properties) const;
    void loadStagingBuffer(const void* data, size_t size, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory) const;
    void copyBuffer(const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, size_t size) const;
    void loadBuffer(const void* data, size_t size, const vk::BufferUsageFlags& usage, vk::Buffer& dstBuffer, vk::DeviceMemory& bufferMemory, vk::Buffer& stagingBuffer, vk::DeviceMemory& stagingBufferMemory, bool keepStagingMemory) const;
    void copyToStagingBuffer(const void* data, size_t size, const vk::DeviceMemory& bufferMemory) const;
    void createDescriptorSet(vk::DescriptorSet& descriptorSet);
    void updateDescriptorSet(const std::vector<vk::WriteDescriptorSet>& descriptorWrites);
    void createTextureImage(const void* data, uint32_t width, uint32_t height, vk::Format format, vk::Image& textureImage, vk::DeviceMemory& textureImageMemory, vk::Buffer& stagingBuffer, vk::DeviceMemory& stagingBufferMemory);
    void transitionImageLayout(vk::Image& image, vk::Format& format, const vk::ImageLayout& oldLayout, const vk::ImageLayout& newLayout);
    void createImage(uint32_t width, uint32_t height, vk::Format& format, const vk::ImageTiling& tiling, const vk::ImageUsageFlags& usage, const vk::MemoryPropertyFlags& properties, vk::Image& image, vk::DeviceMemory& imageMemory);
    static void createImageView(vk::Device& device, const vk::Image& image, const vk::Format& format, const vk::ImageAspectFlags& aspectFlags, vk::ImageView& imageView);
    void createTextureImageView(const vk::Image& image, const vk::Format& format, vk::ImageView& imageView);
    void copyBufferToImage(vk::Buffer& buffer, vk::Image& image, uint32_t width, uint32_t height);
    void createTextureSampler(vk::Sampler& textureSampler);
};

#endif // MEMORYMANAGER_H
