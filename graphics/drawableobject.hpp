#ifndef DRAWABLEOBJECT_H
#define DRAWABLEOBJECT_H
#include "graphicsvertex.hpp"
#include "memoryobject.hpp"
#include "texturememoryobject.hpp"
#include <memory>
#include <vector>

class DrawablePrimitive {
    struct MVP {
        glm::mat4 model;
        glm::mat4 view;
        glm::mat4 proj;
    };
    TextureMemoryObject _texture;
    bool _hasTexture;
    TextureMemoryObject _normal;
    bool _hasNormal;
    std::vector<GraphicsVertex> _assetVertices;
    std::vector<uint32_t> _assetIndices;
    vk::DescriptorSet _descriptorSet;
    MemoryObject _vertexObject;
    MemoryObject _indexObject;

public:
    DrawablePrimitive();
    std::vector<GraphicsVertex> assetVertices() const;
    void setAssetVertices(const std::vector<GraphicsVertex>& assetVertices);
    std::vector<uint32_t> assetIndices() const;
    void setAssetIndices(const std::vector<uint32_t>& assetIndices);
    void freeMemory(vk::Device& device);
    void loadData(MemoryManager& manager, const vk::Buffer& mvp, const vk::Buffer& localTransform);
    void recordCommandBuffer(vk::CommandBuffer& commandBuffer, const vk::PipelineLayout& layout) const;
    TextureMemoryObject texture() const;
    void setTexture(const TextureMemoryObject& texture);
    bool hasTexture() const;
    void setHasTexture(bool hasTexture);
    TextureMemoryObject normal() const;
    void setNormal(const TextureMemoryObject& normal);
    bool hasNormal() const;
    void setHasNormal(bool hasNormal);
};

class DrawableObject {
    glm::mat4 _matrixParent;
    MemoryObject _uniformObject;
    vk::Buffer _mvpBuffer;
    std::vector<DrawablePrimitive> _primitives;
    std::vector<DrawableObject> _children;

public:
    DrawableObject();

    void setAssetMatrix(const glm::mat4& matrix);
    void freeMemory(vk::Device& device);
    void loadData(MemoryManager& manager);

    size_t getIndicesSize() const;
    vk::DescriptorSet getDescriptorSet() const;
    void setMvpBuffer(const vk::Buffer& mvpBuffer);
    glm::mat4 getMatrix() const;
    void setMatrix(const glm::mat4& matrix);
    void recordCommandBuffer(vk::CommandBuffer& commandBuffer, const vk::PipelineLayout& layout) const;
    void addChild(const DrawableObject& object);
    void addPrimitive(const DrawablePrimitive& primitive);
    void applyParentTransformation(const glm::mat4& parent);
    void setTranslation(glm::vec3 pos);
};

#endif // DRAWABLEOBJECT_H
