#ifndef CAMERA_HPP
#define CAMERA_HPP

#include "mvpbufferobject.hpp"
#include <chrono>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

class Camera {
    glm::vec3 _eye;
    glm::vec3 _target;
    glm::vec3 _up;
    glm::mat4 _proj;
    glm::mat4 _model;
    uint32_t _width;
    uint32_t _height;
    glm::vec2 _prevMousePos;
    std::chrono::time_point<std::chrono::high_resolution_clock> _lastTime;
    float _horizontalAngle;

    float _verticalAngle;

public:
    Camera();
    void setupCamera(uint32_t width, uint32_t height);
    MVPBufferObject getMVP();
};

#endif // CAMERA_HPP
