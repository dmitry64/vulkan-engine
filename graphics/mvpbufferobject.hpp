#ifndef MVPBUFFEROBJECT_HPP
#define MVPBUFFEROBJECT_HPP
#include <glm/mat4x4.hpp>

struct MVPBufferObject {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};

#endif // MVPBUFFEROBJECT_HPP
