#include "memorymanager.hpp"

MemoryManager::MemoryManager()
    : _isInited(false)
{
}

void MemoryManager::init(std::shared_ptr<DeviceWrapper> device, vk::PhysicalDevice& physicalDevice, vk::Queue& graphicsQueue, vk::CommandPool& memoryCommandPool, vk::DescriptorPool& memoryDescriptorPool, vk::DescriptorSetLayout& descriptorSetLayout)
{
    _device = device;
    _physicalDevice = physicalDevice;
    _graphicsQueue = graphicsQueue;
    _memoryCommandPool = memoryCommandPool;
    _memoryDescriptorPool = memoryDescriptorPool;
    _descriptorSetLayout = descriptorSetLayout;
    _isInited = true;
}

void MemoryManager::createBuffer(size_t size, const vk::BufferUsageFlags& usage, const vk::MemoryPropertyFlags& properties, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory) const
{
    assert(size != 0);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::BufferCreateInfo bufferInfo;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = vk::SharingMode::eExclusive;

    _device->lock();
    if (_device->getDevice().createBuffer(&bufferInfo, nullptr, &buffer) != vk::Result::eSuccess) {
        std::cerr << "Failed to create buffer!" << std::endl;
        std::abort();
    }

    vk::MemoryRequirements memRequirements;
    _device->getDevice().getBufferMemoryRequirements(buffer, &memRequirements);

    vk::MemoryAllocateInfo allocInfo(memRequirements.size, findMemoryType(memRequirements.memoryTypeBits, properties));

    if (_device->getDevice().allocateMemory(&allocInfo, nullptr, &bufferMemory) != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate buffer memory!" << std::endl;
        std::abort();
    }
    _device->getDevice().bindBufferMemory(buffer, bufferMemory, 0);
    _device->unlock();
}

uint32_t MemoryManager::findMemoryType(uint32_t typeFilter, const vk::MemoryPropertyFlags& properties) const
{
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::PhysicalDeviceMemoryProperties memProperties;
    _physicalDevice.getMemoryProperties(&memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }
    std::cerr << "Failed to find suitable memory type!" << std::endl;
    std::abort();
    return 0;
}

void MemoryManager::copyToStagingBuffer(const void* data, size_t size, const vk::DeviceMemory& bufferMemory) const
{
    assert(data != nullptr);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    void* dataPtr = nullptr;
    _device->lock();
    vk::Result res = _device->getDevice().mapMemory(bufferMemory, 0, size, vk::MemoryMapFlags(), &dataPtr);
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to map memory for index buffer! error:" << res << std::endl;
        std::abort();
    }
    std::memcpy(dataPtr, data, size);
    _device->getDevice().unmapMemory(bufferMemory);
    _device->unlock();
}

void MemoryManager::loadStagingBuffer(const void* data, size_t size, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory) const
{
    assert(data != nullptr);
    assert(size != 0);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }

    createBuffer(size, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, buffer, bufferMemory);
    copyToStagingBuffer(data, size, bufferMemory);
}

void MemoryManager::copyBuffer(const vk::Buffer& srcBuffer, const vk::Buffer& dstBuffer, size_t size) const
{
    assert(size != 0);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::CommandBufferAllocateInfo allocInfo(_memoryCommandPool, vk::CommandBufferLevel::ePrimary, 1);

    vk::CommandBuffer commandBuffer;
    _device->lock();
    vk::Result allocationResult = _device->getDevice().allocateCommandBuffers(&allocInfo, &commandBuffer);
    if (allocationResult != vk::Result::eSuccess) {
        std::cerr << "Allocation failed! code" << allocationResult << std::endl;
        std::abort();
    }

    vk::CommandBufferBeginInfo beginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    vk::BufferCopy copyRegion(0, 0, size);

    if (commandBuffer.begin(&beginInfo) == vk::Result::eSuccess) {
        commandBuffer.copyBuffer(srcBuffer, dstBuffer, 1, &copyRegion);
        commandBuffer.end();
    } else {
        std::cerr << "Failed to begin command buffer!" << std::endl;
        std::abort();
    }

    vk::SubmitInfo submitInfo;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vk::FenceCreateInfo fenceCreateInfo = {};
    vk::Fence fence;
    vk::Result fenceCreateRes = _device->getDevice().createFence(&fenceCreateInfo, nullptr, &fence);
    if (fenceCreateRes != vk::Result::eSuccess) {
        std::cerr << "Failed to create fence! error:" << fenceCreateRes << std::endl;
        std::abort();
    }

    vk::Result submitRes = _graphicsQueue.submit(1, &submitInfo, fence);
    if (submitRes != vk::Result::eSuccess) {
        std::cerr << "Graphics queue submit failed! error:" << submitRes << std::endl;
        std::abort();
    }
    vk::Result fenceWaitRes = _device->getDevice().waitForFences(1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
    if (fenceWaitRes != vk::Result::eSuccess) {
        std::cerr << "Wait on fence failed! error:" << fenceWaitRes << std::endl;
        std::abort();
    }
    _device->getDevice().destroyFence(fence, nullptr);
    _device->getDevice().freeCommandBuffers(_memoryCommandPool, 1, &commandBuffer);
    _device->unlock();
}

void MemoryManager::loadBuffer(const void* data, size_t size, const vk::BufferUsageFlags& usage, vk::Buffer& dstBuffer, vk::DeviceMemory& bufferMemory, vk::Buffer& stagingBuffer, vk::DeviceMemory& stagingBufferMemory, bool keepStagingMemory) const
{
    assert(data != nullptr);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    loadStagingBuffer(data, size, stagingBuffer, stagingBufferMemory);
    createBuffer(size, usage, vk::MemoryPropertyFlagBits::eDeviceLocal, dstBuffer, bufferMemory);
    copyBuffer(stagingBuffer, dstBuffer, size);
    if (!keepStagingMemory) {
        _device->lock();
        _device->getDevice().destroyBuffer(stagingBuffer);
        _device->getDevice().freeMemory(stagingBufferMemory);
        _device->unlock();
    }
}

void MemoryManager::createDescriptorSet(vk::DescriptorSet& descriptorSet)
{
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::DescriptorSetAllocateInfo allocInfo(_memoryDescriptorPool, 1, &_descriptorSetLayout);
    _device->lock();
    if (_device->getDevice().allocateDescriptorSets(&allocInfo, &descriptorSet) != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate descriptor set!" << std::endl;
        std::abort();
    }
    _device->unlock();
}

void MemoryManager::updateDescriptorSet(const std::vector<vk::WriteDescriptorSet>& descriptorWrites)
{
    _device->lock();
    _device->getDevice().updateDescriptorSets(static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    _device->unlock();
}

void MemoryManager::createTextureImage(const void* data, uint32_t width, uint32_t height, vk::Format format, vk::Image& textureImage, vk::DeviceMemory& textureImageMemory, vk::Buffer& stagingBuffer, vk::DeviceMemory& stagingBufferMemory)
{
    assert(data != nullptr);
    size_t size = 4 * height * width;
    assert(size != 0);
    std::cerr << "Loading image size: " << size << " width: " << width << " height: " << height << std::endl;
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    loadStagingBuffer(data, size, stagingBuffer, stagingBufferMemory);
    std::cerr << "CREATE" << std::endl;
    createImage(width, height, format, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, vk::MemoryPropertyFlagBits::eDeviceLocal, textureImage, textureImageMemory);
    std::cerr << "AFTER CREATE" << std::endl;
    transitionImageLayout(textureImage, format, vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal);
    std::cerr << "BEFORE COPY" << std::endl;
    copyBufferToImage(stagingBuffer, textureImage, width, height);
    std::cerr << "AFTER COPY" << std::endl;
    transitionImageLayout(textureImage, format, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal);
    _device->lock();
    _device->getDevice().destroyBuffer(stagingBuffer);
    _device->getDevice().freeMemory(stagingBufferMemory);
    _device->unlock();
}

void MemoryManager::transitionImageLayout(vk::Image& image, vk::Format& format, const vk::ImageLayout& oldLayout, const vk::ImageLayout& newLayout)
{
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::CommandBufferAllocateInfo allocInfo;
    allocInfo.level = vk::CommandBufferLevel::ePrimary;
    allocInfo.commandPool = _memoryCommandPool;
    allocInfo.commandBufferCount = 1;

    vk::CommandBuffer commandBuffer;
    _device->lock();
    _device->getDevice().allocateCommandBuffers(&allocInfo, &commandBuffer);
    _device->unlock();

    vk::CommandBufferBeginInfo beginInfo;
    beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
    commandBuffer.begin(&beginInfo);

    vk::ImageMemoryBarrier barrier;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;

    if (newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
        barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;

        if (format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint) {
            barrier.subresourceRange.aspectMask |= vk::ImageAspectFlagBits::eStencil;
        }
    } else {
        barrier.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
    }

    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = 1;

    vk::PipelineStageFlags sourceStage;
    vk::PipelineStageFlags destinationStage;

    if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal) {
        barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
        sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
        destinationStage = vk::PipelineStageFlagBits::eTransfer;
    } else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
        barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
        barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
        sourceStage = vk::PipelineStageFlagBits::eTransfer;
        destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
    } else if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
        barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite;
        sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
        destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
    } else {
        std::cout << "Failed to transition layout!" << std::endl;
        std::abort();
    }

    commandBuffer.pipelineBarrier(sourceStage, destinationStage, vk::DependencyFlags(), 0, nullptr, 0, nullptr, 1, &barrier);
    commandBuffer.end();

    vk::SubmitInfo submitInfo = {};
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    _graphicsQueue.submit(1, &submitInfo, nullptr);
    _graphicsQueue.waitIdle();
    _device->lock();
    _device->getDevice().freeCommandBuffers(_memoryCommandPool, 1, &commandBuffer);
    _device->unlock();
}

void MemoryManager::createImage(uint32_t width, uint32_t height, vk::Format& format, const vk::ImageTiling& tiling, const vk::ImageUsageFlags& usage, const vk::MemoryPropertyFlags& properties, vk::Image& image, vk::DeviceMemory& imageMemory)
{
    assert(width != 0);
    assert(height != 0);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::ImageCreateInfo imageInfo = {};
    imageInfo.imageType = vk::ImageType::e2D;
    imageInfo.extent.width = width;
    imageInfo.extent.height = height;
    imageInfo.extent.depth = 1;
    imageInfo.mipLevels = 1;
    imageInfo.arrayLayers = 1;
    imageInfo.format = format;
    imageInfo.tiling = tiling;
    imageInfo.initialLayout = vk::ImageLayout::ePreinitialized;
    imageInfo.usage = usage;
    imageInfo.samples = vk::SampleCountFlagBits::e1;
    imageInfo.sharingMode = vk::SharingMode::eExclusive;
    _device->lock();
    vk::Result createRes = _device->getDevice().createImage(&imageInfo, nullptr, &image);
    if (createRes != vk::Result::eSuccess) {
        std::cerr << "failed to create image! code:" << createRes << std::endl;
        std::abort();
    }

    vk::MemoryRequirements memRequirements;
    _device->getDevice().getImageMemoryRequirements(image, &memRequirements);

    vk::MemoryAllocateInfo allocInfo;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    if (_device->getDevice().allocateMemory(&allocInfo, nullptr, &imageMemory) != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate image memory!" << std::endl;
        std::abort();
    }
    _device->getDevice().bindImageMemory(image, imageMemory, 0);
    _device->unlock();
}

void MemoryManager::createImageView(vk::Device& device, const vk::Image& image, const vk::Format& format, const vk::ImageAspectFlags& aspectFlags, vk::ImageView& imageView)
{
    vk::ImageViewCreateInfo viewInfo;
    viewInfo.image = image;
    viewInfo.viewType = vk::ImageViewType::e2D;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;

    if (device.createImageView(&viewInfo, nullptr, &imageView) != vk::Result::eSuccess) {
        std::cerr << "Failed to create texture image view!" << std::endl;
        std::abort();
    }
}

void MemoryManager::createTextureImageView(const vk::Image& image, const vk::Format& format, vk::ImageView& imageView)
{
    _device->lock();
    createImageView(_device->getDevice(), image, format, vk::ImageAspectFlagBits::eColor, imageView);
    _device->unlock();
}

void MemoryManager::copyBufferToImage(vk::Buffer& buffer, vk::Image& image, uint32_t width, uint32_t height)
{
    assert(width != 0);
    assert(height != 0);
    if (!_isInited) {
        std::cerr << "Memory manager not inited!" << std::endl;
        std::abort();
    }
    vk::CommandBufferAllocateInfo allocInfo;
    allocInfo.level = vk::CommandBufferLevel::ePrimary;
    allocInfo.commandPool = _memoryCommandPool;
    allocInfo.commandBufferCount = 1;

    vk::CommandBuffer commandBuffer;
    _device->lock();
    vk::Result allocRes = _device->getDevice().allocateCommandBuffers(&allocInfo, &commandBuffer);
    if (allocRes != vk::Result::eSuccess) {
        std::cerr << "Failed to allocate command buffer! error:" << allocRes << std::endl;
    }

    vk::BufferImageCopy region;
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;
    region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = vk::Offset3D(0, 0, 0);
    region.imageExtent = vk::Extent3D(width, height, 1);

    vk::CommandBufferBeginInfo beginInfo;
    beginInfo.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;
    if (commandBuffer.begin(&beginInfo) == vk::Result::eSuccess) {
        commandBuffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);
        commandBuffer.end();
    } else {
        std::abort();
    }

    vk::SubmitInfo submitInfo = {};
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    std::cerr << "Submitting..." << std::endl;
    _graphicsQueue.submit(1, &submitInfo, nullptr);
    _graphicsQueue.waitIdle();
    _device->getDevice().freeCommandBuffers(_memoryCommandPool, 1, &commandBuffer);
    _device->unlock();
}

void MemoryManager::createTextureSampler(vk::Sampler& textureSampler)
{
    vk::SamplerCreateInfo samplerInfo = {};

    samplerInfo.magFilter = vk::Filter::eLinear;
    samplerInfo.minFilter = vk::Filter::eLinear;
    samplerInfo.addressModeU = vk::SamplerAddressMode::eRepeat;
    samplerInfo.addressModeV = vk::SamplerAddressMode::eRepeat;
    samplerInfo.addressModeW = vk::SamplerAddressMode::eRepeat;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = vk::BorderColor::eIntOpaqueBlack;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = vk::CompareOp::eAlways;
    samplerInfo.mipmapMode = vk::SamplerMipmapMode::eLinear;
    _device->lock();
    if (_device->getDevice().createSampler(&samplerInfo, nullptr, &textureSampler) != vk::Result::eSuccess) {
        std::cerr << "failed to create texture sampler!";
        std::abort();
    }
    _device->unlock();
}
