#include "mainthread.hpp"
#include <chrono>
#include <iostream>

void MainThread::tick(long deltaTime)
{
    handleInput();
}

void MainThread::init()
{
    std::cerr << "Main thread started..." << std::endl;
}

void MainThread::destroy()
{
    std::cerr << "Main thread finished!" << std::endl;
}

void MainThread::handleInput()
{
}

MainThread::MainThread()
    : _runSwitch(true)
    , _thread(&MainThread::run, this)
{
}

void MainThread::run()
{
    init();
    auto start = std::chrono::system_clock::now();
    while (_runSwitch.load()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(6));
        auto end = std::chrono::system_clock::now();
        auto elapsed = end - start;
        start = end;
        tick(elapsed.count());
    }
    destroy();
}

void MainThread::stop()
{
    _runSwitch.store(false);
    _thread.join();
}
