#include "inputhandler.hpp"
#include <iostream>

InputHandler::InputHandler()
{

    std::cerr << "Input handler initialized!" << std::endl;
}

glm::vec2 InputHandler::mousePos() const
{
    return _mousePos;
}

InputHandler& InputHandler::Instance()
{
    static InputHandler instance;
    return instance;
}

void InputHandler::handleMousePos(double x, double y)
{
    _mousePos.x = x;
    _mousePos.y = y;
}

void InputHandler::handleMouseButtonEvent()
{
}
