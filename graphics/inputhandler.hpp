#ifndef INPUTHANDLER_HPP
#define INPUTHANDLER_HPP

#include <glm/vec2.hpp>

class InputHandler {

private:
    InputHandler();

    glm::vec2 _mousePos;

public:
    static InputHandler& Instance();
    void handleMousePos(double x, double y);
    void handleMouseButtonEvent();
    glm::vec2 mousePos() const;
};

#endif // INPUTHANDLER_HPP
