#include "drawableobject.hpp"
#include "memorymanager.hpp"

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/quaternion.hpp"

void DrawableObject::setAssetMatrix(const glm::mat4& matrix)
{
    _matrixParent = matrix;
}

void DrawableObject::freeMemory(vk::Device& device)
{
    _uniformObject.freeMemory(device);

    for (auto& primitive : _primitives) {
        primitive.freeMemory(device);
    }

    for (auto& child : _children) {
        child.freeMemory(device);
    }
}

void DrawableObject::loadData(MemoryManager& manager)
{
    _uniformObject.loadToDevice(reinterpret_cast<const void*>(&_matrixParent), sizeof(glm::mat4), vk::BufferUsageFlagBits::eUniformBuffer, manager);
    for (auto& primitive : _primitives) {
        primitive.loadData(manager, _mvpBuffer, _uniformObject.buffer());
    }

    for (auto& child : _children) {
        child.applyParentTransformation(_matrixParent);
        child.loadData(manager);
    }
}

void DrawableObject::setMvpBuffer(const vk::Buffer& mvpBuffer)
{
    _mvpBuffer = mvpBuffer;
    for (auto& child : _children) {
        child.setMvpBuffer(mvpBuffer);
    }
}

glm::mat4 DrawableObject::getMatrix() const
{
    return _matrixParent;
}

void DrawableObject::setMatrix(const glm::mat4& matrix)
{
    _matrixParent = matrix;
}

void DrawableObject::recordCommandBuffer(vk::CommandBuffer& commandBuffer, const vk::PipelineLayout& layout) const
{
    for (const auto& primitive : _primitives) {
        primitive.recordCommandBuffer(commandBuffer, layout);
    }
    for (const auto& child : _children) {
        child.recordCommandBuffer(commandBuffer, layout);
    }
}

void DrawableObject::addChild(const DrawableObject& object)
{
    _children.push_back(object);
}

void DrawableObject::addPrimitive(const DrawablePrimitive& primitive)
{
    _primitives.push_back(primitive);
}

void DrawableObject::applyParentTransformation(const glm::mat4& parent)
{
    _matrixParent = parent * _matrixParent;
}

void DrawableObject::setTranslation(glm::vec3 pos)
{
    _matrixParent[3][0] = pos.x;
    _matrixParent[3][1] = pos.y;
    _matrixParent[3][2] = pos.z;
}

DrawableObject::DrawableObject()
    : _matrixParent(glm::mat4(1.0f))

{
}

std::vector<uint32_t> DrawablePrimitive::assetIndices() const
{
    return _assetIndices;
}

void DrawablePrimitive::setAssetIndices(const std::vector<uint32_t>& assetIndices)
{
    _assetIndices = assetIndices;
}

void DrawablePrimitive::freeMemory(vk::Device& device)
{
    _vertexObject.freeMemory(device);
    _indexObject.freeMemory(device);
    if (_hasTexture) {
        _texture.freeMemory(device);
        _hasTexture = false;
    }
    if (_hasNormal) {
        _normal.freeMemory(device);
        _hasNormal = false;
    }
    _assetIndices.clear();
    _assetVertices.clear();
}

void DrawablePrimitive::loadData(MemoryManager& manager, const vk::Buffer& mvp, const vk::Buffer& localTransform)
{
    if (!_assetVertices.empty() && !_assetIndices.empty()) {
        _vertexObject.loadToDevice(reinterpret_cast<const void*>(_assetVertices.data()), sizeof(GraphicsVertex) * _assetVertices.size(), vk::BufferUsageFlagBits::eVertexBuffer, manager);
        _indexObject.loadToDevice(reinterpret_cast<const void*>(_assetIndices.data()), sizeof(uint32_t) * _assetIndices.size(), vk::BufferUsageFlagBits::eIndexBuffer, manager);

        if (_hasTexture) {
            _texture.loadData(manager);
        } else {
            _texture.resetImageToDummy();
            _texture.loadData(manager);
        }
        if (_hasNormal) {
            _normal.loadData(manager);
        } else {
            _normal.resetImageToDummy();
            _normal.loadData(manager);
        }

        manager.createDescriptorSet(_descriptorSet);

        vk::DescriptorBufferInfo mvpBufferInfo(mvp, 0, sizeof(MVP));
        vk::DescriptorBufferInfo objectTransformBufferInfo(localTransform, 0, sizeof(glm::mat4));
        vk::DescriptorImageInfo textureImageInfo;
        vk::DescriptorImageInfo normalImageInfo;
        //  if (_hasTexture) {
        textureImageInfo = vk::DescriptorImageInfo(_texture.textureSampler(), _texture.textureImageView(), vk::ImageLayout::eShaderReadOnlyOptimal);
        //   }
        // if (_hasNormal) {
        normalImageInfo = vk::DescriptorImageInfo(_normal.textureSampler(), _normal.textureImageView(), vk::ImageLayout::eShaderReadOnlyOptimal);
        //}

        std::vector<vk::WriteDescriptorSet> descriptorWrites;

        vk::WriteDescriptorSet mvpDescriptor;
        mvpDescriptor.dstSet = _descriptorSet;
        mvpDescriptor.dstBinding = 0;
        mvpDescriptor.dstArrayElement = 0;
        mvpDescriptor.descriptorType = vk::DescriptorType::eUniformBuffer;
        mvpDescriptor.descriptorCount = 1;
        mvpDescriptor.pBufferInfo = &mvpBufferInfo;
        descriptorWrites.push_back(mvpDescriptor);

        vk::WriteDescriptorSet localTrnsformDescriptor;
        localTrnsformDescriptor.dstSet = _descriptorSet;
        localTrnsformDescriptor.dstBinding = 1;
        localTrnsformDescriptor.dstArrayElement = 0;
        localTrnsformDescriptor.descriptorType = vk::DescriptorType::eUniformBuffer;
        localTrnsformDescriptor.descriptorCount = 1;
        localTrnsformDescriptor.pBufferInfo = &objectTransformBufferInfo;
        descriptorWrites.push_back(localTrnsformDescriptor);

        // if (_hasTexture) {
        vk::WriteDescriptorSet textureDescriptor;
        textureDescriptor.dstSet = _descriptorSet;
        textureDescriptor.dstBinding = 2;
        textureDescriptor.dstArrayElement = 0;
        textureDescriptor.descriptorType = vk::DescriptorType::eCombinedImageSampler;
        textureDescriptor.descriptorCount = 1;
        textureDescriptor.pImageInfo = &textureImageInfo;
        descriptorWrites.push_back(textureDescriptor);
        // }

        //  if (_hasNormal) {
        vk::WriteDescriptorSet normalDescriptor;
        normalDescriptor.dstSet = _descriptorSet;
        normalDescriptor.dstBinding = 3;
        normalDescriptor.dstArrayElement = 0;
        normalDescriptor.descriptorType = vk::DescriptorType::eCombinedImageSampler;
        normalDescriptor.descriptorCount = 1;
        normalDescriptor.pImageInfo = &normalImageInfo;
        descriptorWrites.push_back(normalDescriptor);
        //  }

        manager.updateDescriptorSet(descriptorWrites);
    }
}

void DrawablePrimitive::recordCommandBuffer(vk::CommandBuffer& commandBuffer, const vk::PipelineLayout& layout) const
{
    if (!_assetIndices.empty()) {
        vk::DeviceSize offsets[] = { 0 };
        vk::Buffer vertexBuffers[] = { _vertexObject.buffer() };
        vk::DescriptorSet descriptorSets[] = { _descriptorSet };
        commandBuffer.bindVertexBuffers(0, 1, vertexBuffers, offsets);
        commandBuffer.bindIndexBuffer(_indexObject.buffer(), 0, vk::IndexType::eUint32);
        commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, layout, 0, 1, descriptorSets, 0, nullptr);
        commandBuffer.drawIndexed(static_cast<uint32_t>(_assetIndices.size()), 1, 0, 0, 0);
    }
}

TextureMemoryObject DrawablePrimitive::texture() const
{
    return _texture;
}

void DrawablePrimitive::setTexture(const TextureMemoryObject& texture)
{
    _texture = texture;
}

bool DrawablePrimitive::hasTexture() const
{
    return _hasTexture;
}

void DrawablePrimitive::setHasTexture(bool hasTexture)
{
    _hasTexture = hasTexture;
}

TextureMemoryObject DrawablePrimitive::normal() const
{
    return _normal;
}

void DrawablePrimitive::setNormal(const TextureMemoryObject& normal)
{
    _normal = normal;
}

bool DrawablePrimitive::hasNormal() const
{
    return _hasNormal;
}

void DrawablePrimitive::setHasNormal(bool hasNormal)
{
    _hasNormal = hasNormal;
}

DrawablePrimitive::DrawablePrimitive()
    : _hasTexture(false)
    , _hasNormal(false)
{
}

std::vector<GraphicsVertex> DrawablePrimitive::assetVertices() const
{
    return _assetVertices;
}

void DrawablePrimitive::setAssetVertices(const std::vector<GraphicsVertex>& assetVertices)
{
    _assetVertices = assetVertices;
}
