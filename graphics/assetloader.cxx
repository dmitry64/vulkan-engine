#include "assetloader.hpp"
#define TINYGLTF_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../tinygltf/tiny_gltf.h"
#include "helperfunctions.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/ext.hpp"
#include "glm/gtx/quaternion.hpp"
AssetLoader::AssetLoader()
{
}

void AssetLoader::copyPositionVertices(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts)
{
    assert(!floats.empty());

    if (assetVerts.empty()) {
        assetVerts.resize(floats.size() / 3);
    }
    std::cerr << "   PositionVertices SIZE: " << floats.size() / 3 << " verts: " << assetVerts.size() << std::endl;
    assert(assetVerts.size() == floats.size() / 3);

    size_t index = 0;
    for (size_t i = 0; i < floats.size(); i += 3) {
        glm::vec3 vec(floats[i], floats[i + 1], floats[i + 2]);
        assetVerts[index].pos = vec;
        ++index;
    }
}

void AssetLoader::copyColors(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts)
{
    assert(!floats.empty());
    if (assetVerts.empty()) {
        assetVerts.resize(floats.size() / 3);
    }
    std::cerr << "   Colors SIZE: " << floats.size() / 3 << " verts: " << assetVerts.size() << std::endl;
    assert(assetVerts.size() == floats.size() / 3);

    size_t index = 0;
    for (size_t i = 0; i < floats.size(); i += 3) {
        glm::vec3 vec(floats[i], floats[i + 1], floats[i + 2]);
        assetVerts[index].color = vec;
        ++index;
    }
}

void AssetLoader::copyNormals(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts)
{
    assert(!floats.empty());
    if (assetVerts.empty()) {
        assetVerts.resize(floats.size() / 3);
    }
    std::cerr << "   Normals SIZE: " << floats.size() / 3 << " verts: " << assetVerts.size() << std::endl;
    assert(assetVerts.size() == floats.size() / 3);

    size_t index = 0;
    for (size_t i = 0; i < floats.size(); i += 3) {
        glm::vec3 vec(floats[i], floats[i + 1], floats[i + 2]);
        assetVerts[index].normal = vec;
        ++index;
    }
}

void AssetLoader::copyTexCoords(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts)
{
    assert(!floats.empty());
    if (assetVerts.empty()) {
        assetVerts.resize(floats.size() / 2);
    }
    std::cerr << "   TexCoords SIZE: " << floats.size() / 2 << " verts: " << assetVerts.size() << std::endl;
    assert(assetVerts.size() == floats.size() / 2);

    size_t index = 0;
    for (size_t i = 0; i < floats.size(); i += 2) {
        glm::vec2 vec(floats[i], floats[i + 1]);
        assetVerts[index].texCoord = vec;
        ++index;
    }
}

void AssetLoader::handleNode(int nodeId, tinygltf::Model& model, DrawableObject& parent, const std::vector<TextureMemoryObject>& textures)
{
    const auto& node = model.nodes.at(nodeId);

    DrawableObject drawableObject;
    std::cerr << " NODE: [" << nodeId << "] " << node.name << std::endl;
    auto& matrix = node.matrix;
    if (!matrix.empty()) {
        assert(matrix.size() == 16);
        glm::mat4 mat;
        int index = 0;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                mat[i][j] = matrix.at(index);
                index++;
            }
        }
        drawableObject.setAssetMatrix(mat);
    } else {
        auto& translation = node.translation;
        glm::vec3 trans(0.0f, 0.0f, 0.0f);
        if (!translation.empty()) {
            trans = glm::vec3(translation.at(0), translation.at(1), translation.at(2));
        }
        auto& rotation = node.rotation;
        glm::vec4 rot(0.0f, 0.0f, 0.0f, 1.0f);
        if (!rotation.empty()) {
            rot = glm::vec4(rotation.at(0), rotation.at(1), rotation.at(2), rotation.at(3));
        }
        auto& scale = node.scale;
        glm::vec3 sc(1.0f, 1.0f, 1.0f);
        if (!scale.empty()) {
            sc = glm::vec3(scale.at(0), scale.at(1), scale.at(2));
        }
        glm::mat4 mat(1.0f);
        glm::mat4 tmat = glm::translate(glm::mat4(1.0f), trans);
        glm::mat4 smat = glm::scale(glm::mat4(1.0f), sc);
        glm::quat myquat(rot.w, rot.x, rot.y, rot.z);
        glm::mat4 rmat = glm::mat4(myquat);
        mat = tmat * rmat * smat;
        std::cerr << "T:" << glm::to_string(tmat) << std::endl;
        std::cerr << "R:" << glm::to_string(rmat) << std::endl;
        std::cerr << "S:" << glm::to_string(smat) << std::endl;
        std::cerr << "O:" << glm::to_string(mat) << std::endl;

        drawableObject.setAssetMatrix(mat);
    }

    if (node.mesh >= 0) {
        const auto& mesh = model.meshes.at(node.mesh);
        std::cerr << " MESH: [" << node.mesh << "] " << mesh.name << std::endl;
        for (auto primitive : mesh.primitives) {
            DrawablePrimitive object;
            std::cerr << "  PRIMITIVE mode: " << primitive.mode << " indices: " << primitive.indices << " material: " << primitive.material << std::endl;
            assert(primitive.mode == TINYGLTF_MODE_TRIANGLES);
            auto& material = model.materials.at(primitive.material);

            std::cerr << "  MATERIAL: " << material.name << std::endl;
            for (const auto& value : material.values) {
                std::cerr << "   PARAM: " << value.first << " VAL: " << value.second.TextureIndex() << std::endl;
                if (value.first == "baseColorTexture") {
                    int id = value.second.TextureIndex();
                    if (id >= 0) {
                        const auto& texture = textures.at(id);
                        object.setTexture(texture);
                        object.setHasTexture(true);
                    }
                }
            }

            std::cerr << "  ADDITIONAL: " << material.name << std::endl;
            for (const auto& value : material.additionalValues) {
                std::cerr << "   PARAM: " << value.first << " VAL: " << value.second.TextureIndex() << std::endl;
                if (value.first == "normalTexture") {
                    int id = value.second.TextureIndex();
                    if (id >= 0) {
                        const auto& texture = textures.at(id);
                        object.setNormal(texture);
                        object.setHasNormal(true);
                    }
                }
            }
            auto& indexAccessor = model.accessors.at(primitive.indices);
            std::cerr << "  ACCESSOR: " << indexAccessor.name << " OFFSET: " << indexAccessor.byteOffset << " TYPE: " << indexAccessor.type << " COMPTYPE: " << indexAccessor.componentType << " BUFVIEW: " << indexAccessor.bufferView << std::endl;
            auto& indexBufferView = model.bufferViews.at(indexAccessor.bufferView);
            std::cerr << "  BUFFER VIEW: " << indexBufferView.name << " OFFSET:" << indexBufferView.byteOffset << " LENGTH:" << indexBufferView.byteLength << " STRIDE:" << indexBufferView.byteStride << std::endl;
            auto& indexBuffer = model.buffers.at(indexBufferView.buffer);
            std::cerr << "  BUFFER: " << indexBuffer.name << " URI:" << indexBuffer.uri << std::endl;
            const std::vector<unsigned char>& data = indexBuffer.data;
            size_t indexSize = indexBufferView.byteLength - indexAccessor.byteOffset;
            std::cerr << "  UNSIGNED SIZE: " << indexSize << std::endl;
            std::vector<unsigned char> tempIndexBufferView(indexBufferView.byteLength);
            std::memcpy(tempIndexBufferView.data(), data.data() + indexBufferView.byteOffset, indexBufferView.byteLength);
            assert(indexAccessor.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT || indexAccessor.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT);

            std::vector<uint32_t> tempIndices;
            if (indexAccessor.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT) {
                size_t elementSize = sizeof(uint16_t);
                size_t indexStep = indexBufferView.byteStride == 0 ? elementSize : indexBufferView.byteStride;
                size_t totalSize = indexAccessor.count * indexStep;
                tempIndices.resize(indexAccessor.count);
                size_t index = 0;
                for (size_t i = 0; i < totalSize; i += indexStep) {
                    tempIndices[index] = (*reinterpret_cast<const uint16_t*>(tempIndexBufferView.data() + indexAccessor.byteOffset + i));
                    ++index;
                }
            } else if (indexAccessor.componentType == TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT) {
                size_t elementSize = sizeof(uint32_t);
                size_t indexStep = indexBufferView.byteStride == 0 ? elementSize : indexBufferView.byteStride;
                size_t totalSize = indexAccessor.count * indexStep;
                tempIndices.resize(indexAccessor.count);
                size_t index = 0;
                for (size_t i = 0; i < totalSize; i += indexStep) {
                    tempIndices[index] = (*reinterpret_cast<const uint32_t*>(tempIndexBufferView.data() + indexAccessor.byteOffset + i));
                    ++index;
                }
            }

            std::cerr << std::endl;
            std::vector<GraphicsVertex> tempVertices;

            for (auto attributeName : primitive.attributes) {
                std::cerr << "   ATTRIB: " << attributeName.first << " id: " << attributeName.second << std::endl;
                auto& accessor = model.accessors.at(attributeName.second);
                std::cerr << "   ACCESSOR: " << accessor.name << " OFFSET: " << accessor.byteOffset << " TYPE: " << accessor.type << " COMPTYPE: " << accessor.componentType << " BUFVIEW: " << accessor.bufferView << std::endl;
                auto& bufferView = model.bufferViews.at(accessor.bufferView);
                std::cerr << "   BUFFER VIEW: " << bufferView.name << " OFFSET:" << bufferView.byteOffset << " LENGTH:" << bufferView.byteLength << " STRIDE:" << bufferView.byteStride << std::endl;
                auto& buffer = model.buffers.at(bufferView.buffer);
                std::cerr << "   BUFFER: " << buffer.name << " URI:" << buffer.uri << std::endl;
                const std::vector<unsigned char>& data = buffer.data;
                std::cerr << "   DATA SIZE: " << data.size() << std::endl;
                std::vector<unsigned char> tempBufferView(bufferView.byteLength);
                std::memcpy(tempBufferView.data(), data.data() + bufferView.byteOffset, bufferView.byteLength);

                if (accessor.componentType == TINYGLTF_COMPONENT_TYPE_FLOAT) {
                    std::cerr << "   COPYING VERTICES!" << std::endl;
                    std::vector<float> floats;

                    switch (accessor.type) {
                    case TINYGLTF_TYPE_VEC3: {
                        std::cerr << "   Copying floats vec3! size bytes:" << tempBufferView.size() << " ACCESSOR OFFSET:" << accessor.byteOffset << " COUNT: " << accessor.count << std::endl;
                        size_t step = (bufferView.byteStride == 0) ? 12 : bufferView.byteStride;
                        size_t totalSize = accessor.count * step;
                        floats.resize(accessor.count * 3);
                        size_t index = 0;
                        for (size_t i = 0; i < totalSize; i += step) {
                            floats[index] = (*reinterpret_cast<const float*>(tempBufferView.data() + accessor.byteOffset + i));
                            ++index;
                            floats[index] = (*reinterpret_cast<const float*>(tempBufferView.data() + accessor.byteOffset + i + 4));
                            ++index;
                            floats[index] = (*reinterpret_cast<const float*>(tempBufferView.data() + accessor.byteOffset + i + 8));
                            ++index;
                        }
                    } break;
                    case TINYGLTF_TYPE_VEC2: {
                        std::cerr << "    Copying floats vec2! size bytes:" << tempBufferView.size() << " ACCESSOR OFFSET:" << accessor.byteOffset << " COUNT: " << accessor.count << std::endl;
                        size_t step = (bufferView.byteStride == 0) ? 8 : bufferView.byteStride;
                        size_t totalSize = accessor.count * step;
                        floats.resize(accessor.count * 2);
                        size_t index = 0;
                        for (size_t i = 0; i < totalSize; i += step) {
                            floats[index] = (*reinterpret_cast<const float*>(tempBufferView.data() + accessor.byteOffset + i));
                            ++index;
                            floats[index] = (*reinterpret_cast<const float*>(tempBufferView.data() + accessor.byteOffset + i + 4));
                            ++index;
                        }
                    } break;
                    default:
                        std::cerr << "Unsupported vertex format!" << std::endl;
                        break;
                    }

                    if (attributeName.first == "POSITION") {
                        copyPositionVertices(floats, tempVertices);
                    }
                    if (attributeName.first == "COLOR_0") {
                        copyColors(floats, tempVertices);
                    }
                    if (attributeName.first == "TEXCOORD_0") {
                        copyTexCoords(floats, tempVertices);
                    }

                    if (attributeName.first == "NORMAL") {
                        copyNormals(floats, tempVertices);
                    }
                }
                std::cerr << std::endl;
            }
            object.setAssetIndices(tempIndices);
            object.setAssetVertices(tempVertices);
            drawableObject.addPrimitive(object);
        }
    }

    if (!node.children.empty()) {
        for (auto child : node.children) {
            handleNode(child, model, drawableObject, textures);
        }
    }
    parent.addChild(drawableObject);
}

std::vector<DrawableObject> AssetLoader::loadAssets(const std::string& filePath)
{
    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string err;

    //bool ret = loader.LoadASCIIFromFile(&model, &err, "/home/legion/workspace/glTF-Sample-Models/2.0/GearboxAssy/glTF/GearboxAssy.gltf");
    //bool ret = loader.LoadASCIIFromFile(&model, &err, "/home/legion/workspace/glTF-Sample-Models/2.0/DamagedHelmet/glTF/DamagedHelmet.gltf");
    //bool ret = loader.LoadASCIIFromFile(&model, &err, "/home/legion/workspace/glTF-Sample-Models/2.0/ReciprocatingSaw/glTF/ReciprocatingSaw.gltf");

    //bool ret = loader.LoadASCIIFromFile(&model, &err, "/home/legion/workspace/glTF-Sample-Models/2.0/SciFiHelmet/glTF/SciFiHelmet.gltf");
    //bool ret = loader.LoadASCIIFromFile(&model, &err, "/home/legion/workspace/glTF-Sample-Models/2.0/FlightHelmet/glTF/FlightHelmet.gltf");
    bool ret = loader.LoadASCIIFromFile(&model, &err, filePath);

    if (!ret) {
        std::cerr << "Failed to parse glTF" << std::endl;
        std::abort();
    }
    std::vector<DrawableObject> drawables;
    std::vector<TextureMemoryObject> textures(model.textures.size());
    size_t texturesIndex = 0;
    for (auto& texture : model.textures) {
        std::cerr << "TEXTURE SOURCE: " << texture.source << " SAMPLER: " << texture.sampler << std::endl;

        auto& image = model.images.at(texture.source);
        std::cerr << "IMAGE [" << image.height << "," << image.width << "] " << image.uri << " name: " << image.name << " mime: " << image.mimeType << " comp: " << image.component << std::endl;

        TextureMemoryObject textureObject;
        textureObject.setHeight(image.height);
        textureObject.setWidth(image.width);

        if (image.component == 4) {
            assert(4 * image.width * image.height == image.image.size());
            textureObject.setImageData(image.image);
        } else if (image.component == 3) {
            assert(3 * image.width * image.height == image.image.size());
            textureObject.setImageData(rgbToRgba(image.image));
        } else {
            std::cerr << "Unsupported image component!" << std::endl;
            std::abort();
        }

        textures[texturesIndex] = textureObject;
        ++texturesIndex;

        std::cerr << "IMAGE pixels: " << image.height * image.width << " bytes: " << image.image.size() << std::endl;
    }

    for (auto scene : model.scenes) {
        std::cerr << "SCENE: " << scene.name << std::endl;
        DrawableObject sceneEntity;
        sceneEntity.setAssetMatrix(glm::mat4(1.0f));

        for (auto nodeId : scene.nodes) {
            handleNode(nodeId, model, sceneEntity, textures);
        }

        drawables.push_back(sceneEntity);
    }

    return drawables;
}
