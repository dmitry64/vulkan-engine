#ifndef MEMORYOBJECT_H
#define MEMORYOBJECT_H

#include <vulkan/vulkan.hpp>

class MemoryManager;

class MemoryObject {
    bool _keepStagingBuffer;
    vk::Buffer _buffer;
    vk::DeviceMemory _bufferMemory;
    vk::Buffer _stagingBuffer;
    vk::DeviceMemory _stagingBufferMemory;

public:
    explicit MemoryObject(bool keepStagingBuffer = false);
    ~MemoryObject();
    vk::Buffer buffer() const;
    vk::DeviceMemory bufferMemory() const;
    bool keepStagingBuffer() const;
    void loadToDevice(const void* data, size_t size, const vk::BufferUsageFlags& usage, const MemoryManager& manager);
    void freeMemory(vk::Device& device);
    void updateData(const void* data, size_t size, const MemoryManager& manager);
    vk::Buffer stagingBuffer() const;
    vk::DeviceMemory stagingBufferMemory() const;
};

#endif // MEMORYOBJECT_H
