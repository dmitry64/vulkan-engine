#ifndef RENDERINGTHREAD_HPP
#define RENDERINGTHREAD_HPP
#include "drawableobject.hpp"
#include <condition_variable>
#include <mutex>
#include <thread>
#include <vulkan/vulkan.hpp>

class RenderThreadPool {

    std::vector<vk::CommandBuffer> _commandBuffers;
    vk::CommandPool _renderCommandPool;
    vk::RenderPass _renderPass;
    std::shared_ptr<DeviceWrapper> _device;
    vk::Pipeline _graphicsPipeline;
    vk::Pipeline _uiPipeline;
    vk::PipelineLayout _pipelineLayout;
    std::vector<DrawableObject> _entities;
    bool _bufferReady;

public:
    RenderThreadPool();
    ~RenderThreadPool();
    void doRender(const vk::RenderPassBeginInfo& renderPassInfo);
    void doCleanup();
    void init(std::shared_ptr<DeviceWrapper> device, vk::Pipeline& graphicsPipeline, vk::Pipeline& uiPipeline, vk::PipelineLayout& layout, vk::RenderPass& renderPass, uint32_t queueIndex);
    void setRenderingData(const std::vector<DrawableObject>& entities);
    std::vector<vk::CommandBuffer> getResults();
    void destroyHandlers();
    void waitForFinished();

private:
    void render(const vk::RenderPassBeginInfo& renderPassInfo);
    void cleanup();
};

#endif // RENDERINGTHREAD_HPP
