#include "application.hpp"
#include "inputhandler.hpp"
#include <algorithm>
#include <chrono>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <set>

Application::Application()
    : _mainThread(nullptr)
    , _uniformObject(true)
{
    auto helmet = _assetLoader.loadAssets("/home/legion/workspace/glTF-Sample-Models/2.0/DamagedHelmet/glTF/DamagedHelmet.gltf");
    //auto helmet2 = _assetLoader.loadAssets("/home/legion/workspace/glTF-Sample-Models/2.0/GearboxAssy/glTF/GearboxAssy.gltf");
    auto helmet3 = _assetLoader.loadAssets("/home/legion/workspace/glTF-Sample-Models/2.0/SciFiHelmet/glTF/SciFiHelmet.gltf");
    //const auto& helmet3 = _assetLoader.loadAssets("/home/legion/workspace/glTF-Sample-Models/2.0/GearboxAssy/glTF/GearboxAssy.gltf");
    //const auto& helmet4 = _assetLoader.loadAssets("/home/legion/workspace/glTF-Sample-Models/2.0/ReciprocatingSaw/glTF/ReciprocatingSaw.gltf");
    //helmet2.front().setTranslation(glm::vec3(2.0f, 0.0f, 0.0f));
    helmet3.front().setTranslation(glm::vec3(-2.0f, 0.0f, 0.0f));
    _entities.insert(_entities.begin(), helmet.begin(), helmet.end());
    // _entities.insert(_entities.begin(), helmet2.begin(), helmet2.end());
    _entities.insert(_entities.begin(), helmet3.begin(), helmet3.end());
    //_entities.insert(_entities.begin(), helmet4.begin(), helmet4.end());
}

Application::~Application() {}

void Application::run()
{
    _window.init();
    InputHandler::Instance();
    initVulkan();
    startMainThread();
    mainLoop();
    stopMainThread();
    destroyVulkan();
    _window.destroy();
}

void Application::initVulkan()
{
    createInstance();
    setupDebugCallback();
    createSurface();
    pickPhysicalDevice();
    createLogicalDevice();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsDescriptorSetLayout();
    createUiDescriptorSetLayout();
    createGraphicsPipeline();
    createUiPipeline();
    createCommandPool();
    createDescriptorPool();
    createMemoryManager();
    createDepthResources();
    createFramebuffers();
    createUniformBuffer();
    updateUniformBuffer();
    createSemaphores();
    loadDrawableObjects();
    initRenderingThread();
}

void Application::mainLoop()
{
    while (!_window.shouldBeClosed()) {
        _window.pollEvents();
        updateUniformBuffer();
        drawFrame();
    }
}

void Application::startMainThread()
{
    _mainThread = std::make_shared<MainThread>();
}

void Application::stopMainThread()
{
    assert(_mainThread != nullptr);
    _mainThread->stop();
}

void Application::initRenderingThread()
{
    _renderingThread.init(_device, _graphicsPipeline, _uiPipeline, _graphicsPipelineLayout, _renderPass, static_cast<uint32_t>(_queueFamilyIndices.graphicsFamily));
}

void Application::destroyVulkan()
{
    _graphicsQueue.waitIdle();
    _presentQueue.waitIdle();
    _device->lock();
    for (const vk::Fence& fence : _waitFences) {
        _device->getDevice().destroyFence(fence);
    }

    for (const vk::Framebuffer& framebuffer : _swapChainFramebuffers) {
        _device->getDevice().destroyFramebuffer(framebuffer);
    }

    for (const vk::ImageView& imageview : _swapChainImageViews) {
        _device->getDevice().destroyImageView(imageview);
    }

    _device->getDevice().destroySemaphore(_imageAvailableSemaphore);
    _device->getDevice().destroySemaphore(_renderFinishedSemaphore);
    _device->getDevice().destroyImage(_depthImage);

    for (auto& entity : _entities) {
        entity.freeMemory(_device->getDevice());
    }
    _uniformObject.freeMemory(_device->getDevice());

    _device->getDevice().freeMemory(_depthImageMemory);
    _device->getDevice().destroyImageView(_depthImageView);
    _device->getDevice().destroyDescriptorSetLayout(_graphicsDescriptorSetLayout);
    _device->getDevice().destroyDescriptorPool(_descriptorPool);
    _device->getDevice().destroyCommandPool(_memoryCommandPool);
    _device->unlock();
    _renderingThread.destroyHandlers();
    _device->lock();
    _device->getDevice().destroyShaderModule(_vertShaderModule);
    _device->getDevice().destroyShaderModule(_fragShaderModule);
    _device->getDevice().destroyPipelineCache(_grapicsCache);
    _device->getDevice().destroyPipelineCache(_uiCache);
    _device->getDevice().destroyPipelineLayout(_graphicsPipelineLayout);
    _device->getDevice().destroyPipeline(_graphicsPipeline);
    _device->getDevice().destroyRenderPass(_renderPass);
    _device->getDevice().destroySwapchainKHR(_swapChain);
    _device->getDevice().destroy();
    _device->unlock();
    DestroyDebugReportCallbackEXT(_instance, _callback, nullptr);
    _instance.destroySurfaceKHR(_surface);
    _instance.destroy();
}

void Application::createInstance()
{
    std::cerr << "Creating instance..." << std::endl;
    if (enableValidationLayers && !checkValidationLayerSupport()) {
        std::cerr << "validation layers requested, but not available!" << std::endl;
        std::abort();
    }

    vk::ApplicationInfo appInfo;
    appInfo.pApplicationName = "Engine";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_1;

    vk::InstanceCreateInfo createInfo;
    createInfo.pApplicationInfo = &appInfo;

    const std::vector<const char*>& extensions = _window.getRequiredExtensions(enableValidationLayers);
    createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
    createInfo.ppEnabledExtensionNames = extensions.data();

    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    } else {
        createInfo.ppEnabledLayerNames = nullptr;
        createInfo.enabledLayerCount = 0;
    }

    vk::Result res = vk::createInstance(&createInfo, nullptr, &_instance);
    if (res != vk::Result::eSuccess) {
        std::cerr << "Cannot create instance! error:" << res << std::endl;
        std::abort();
    }
}

void Application::setupDebugCallback()
{
    if (enableValidationLayers) {
        std::cerr << "Setting up callbacks..." << std::endl;
        vk::DebugReportCallbackCreateInfoEXT createInfo;
        createInfo.flags = vk::DebugReportFlagBitsEXT::eError | vk::DebugReportFlagBitsEXT::eWarning;
        createInfo.pfnCallback = debugCallback;

        VkResult res = CreateDebugReportCallbackEXT(_instance, &createInfo.operator const VkDebugReportCallbackCreateInfoEXT&(), nullptr, &_callback);
        if (res != VK_SUCCESS) {
            std::cerr << "Failed to set up debug callback! error:" << res << std::endl;
            std::abort();
        }
    } else {
        std::cerr << "No callbacks..." << std::endl;
    }
}

void Application::createSurface()
{
    std::cerr << "Creating surface..." << std::endl;
    vk::Result res = _window.createSurface(_instance, _surface);
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to create surface! error:" << res << std::endl;
        std::abort();
    }
}

void Application::pickPhysicalDevice()
{
    std::cerr << "Picking physical device..." << std::endl;
    uint32_t deviceCount = 0;
    vk::Result enumRes = _instance.enumeratePhysicalDevices(&deviceCount, nullptr);
    if (enumRes != vk::Result::eSuccess) {
        std::cerr << "Failed to enumerate physical devices! error:" << enumRes;
        std::abort();
    }

    if (deviceCount == 0) {
        std::cerr << "failed to find GPUs with Vulkan support!" << std::endl;
        std::abort();
    }

    std::vector<vk::PhysicalDevice> devices(deviceCount);
    enumRes = _instance.enumeratePhysicalDevices(&deviceCount, devices.data());
    if (enumRes != vk::Result::eSuccess) {
        std::cerr << "Failed to enumerate physical devices! error:" << enumRes;
        std::abort();
    }
    for (const auto& device : devices) {
        _physicalDevice = device;
        _queueFamilyIndices = findQueueFamilies(_physicalDevice, _surface);
    }

    if (_physicalDevice.operator VkPhysicalDevice_T*() == VK_NULL_HANDLE) {
        std::cerr << "Failed to find a suitable GPU!" << std::endl;
        std::abort();
    }
}

void Application::createLogicalDevice()
{
    std::cerr << "Creating logical device..." << std::endl;

    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
    std::set<int> uniqueQueueFamilies = { _queueFamilyIndices.graphicsFamily, _queueFamilyIndices.presentFamily };

    float queuePriority = 1.0f;
    for (int queueFamily : uniqueQueueFamilies) {
        vk::DeviceQueueCreateInfo queueCreateInfo;
        queueCreateInfo.queueFamilyIndex = static_cast<uint32_t>(queueFamily);
        queueCreateInfo.queueCount = 1;
        queueCreateInfo.pQueuePriorities = &queuePriority;
        queueCreateInfos.push_back(queueCreateInfo);
    }

    vk::PhysicalDeviceFeatures deviceFeatures;
    deviceFeatures.setSamplerAnisotropy(VK_TRUE);
    vk::DeviceCreateInfo createInfo = {};

    createInfo.pQueueCreateInfos = queueCreateInfos.data();
    createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
    createInfo.ppEnabledExtensionNames = deviceExtensions.data();

    if (enableValidationLayers) {
        createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        createInfo.ppEnabledLayerNames = validationLayers.data();
    } else {
        createInfo.enabledLayerCount = 0;
    }
    _device = std::make_shared<DeviceWrapper>();
    _device->lock();
    vk::Result res = _physicalDevice.createDevice(&createInfo, nullptr, &_device->getDevice());
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to create logical device! error:" << res << std::endl;
        std::abort();
    }
    _device->getDevice().getQueue(static_cast<uint32_t>(_queueFamilyIndices.graphicsFamily), 0, &_graphicsQueue);
    _device->getDevice().getQueue(static_cast<uint32_t>(_queueFamilyIndices.presentFamily), 0, &_presentQueue);
    _device->unlock();
}

void Application::createMemoryManager()
{
    std::cerr << "Creating memory manager..." << std::endl;
    _device->lock();
    _memoryManager.init(_device, _physicalDevice, _graphicsQueue, _memoryCommandPool, _descriptorPool, _graphicsDescriptorSetLayout);
    _device->unlock();
}

void Application::createSwapChain()
{
    std::cerr << "Creating swap chain..." << std::endl;
    const SwapChainSupportDetails& swapChainSupport = querySwapChainSupport(_physicalDevice, _surface);

    vk::SurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    vk::PresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
    vk::Extent2D extent = chooseSwapExtent(swapChainSupport.capabilities, _window.width(), _window.height());

    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    vk::SwapchainCreateInfoKHR createInfo = {};
    createInfo.surface = _surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;

    uint32_t queueFamilyIndices[] = { static_cast<uint32_t>(_queueFamilyIndices.graphicsFamily), static_cast<uint32_t>(_queueFamilyIndices.presentFamily) };

    if (_queueFamilyIndices.graphicsFamily != _queueFamilyIndices.presentFamily) {
        createInfo.imageSharingMode = vk::SharingMode::eConcurrent;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = vk::SharingMode::eExclusive;
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = vk::SwapchainKHR();

    vk::SwapchainKHR newSwapChain;
    _device->lock();
    _device->getDevice().waitIdle();
    vk::Result res = _device->getDevice().createSwapchainKHR(&createInfo, nullptr, &newSwapChain);
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to create swap chain! error:" << res << std::endl;
        std::abort();
    }
    _device->getDevice().waitIdle();
    _swapChain = newSwapChain;
    _device->getDevice().getSwapchainImagesKHR(_swapChain, &imageCount, nullptr);
    _swapChainImages.clear();
    _swapChainImages.resize(imageCount);
    _device->getDevice().getSwapchainImagesKHR(_swapChain, &imageCount, _swapChainImages.data());
    _device->unlock();
    _swapChainImageFormat = surfaceFormat.format;
    _swapChainExtent = extent;
    assert(_swapChainExtent.height != 0);
}

void Application::createImageViews()
{
    std::cerr << "Creating image views..." << std::endl;
    _swapChainImageViews.resize(_swapChainImages.size());

    size_t size = _swapChainImages.size();
    _device->lock();
    for (size_t i = 0; i < size; i++) {
        MemoryManager::createImageView(_device->getDevice(), _swapChainImages[i], _swapChainImageFormat, vk::ImageAspectFlagBits::eColor, _swapChainImageViews[i]);
    }
    _device->unlock();
}

void Application::createRenderPass()
{
    std::cerr << "Creating render pass..." << std::endl;
    vk::AttachmentDescription colorAttachment;
    colorAttachment.format = _swapChainImageFormat;
    colorAttachment.samples = vk::SampleCountFlagBits::e1;
    colorAttachment.loadOp = vk::AttachmentLoadOp::eClear;
    colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
    colorAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    colorAttachment.initialLayout = vk::ImageLayout::eUndefined;
    colorAttachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    vk::AttachmentDescription depthAttachment;
    depthAttachment.format = findDepthFormat(_physicalDevice);
    depthAttachment.samples = vk::SampleCountFlagBits::e1;
    depthAttachment.loadOp = vk::AttachmentLoadOp::eClear;
    depthAttachment.storeOp = vk::AttachmentStoreOp::eDontCare;
    depthAttachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    depthAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    depthAttachment.initialLayout = vk::ImageLayout::eUndefined;
    depthAttachment.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

    std::array<vk::AttachmentDescription, 2> attachments = { { colorAttachment, depthAttachment } };

    vk::AttachmentReference colorAttachmentRef(0, vk::ImageLayout::eColorAttachmentOptimal);
    vk::AttachmentReference depthAttachmentRef(1, vk::ImageLayout::eDepthStencilAttachmentOptimal);

    vk::SubpassDescription subpass;
    subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    vk::SubpassDescription subpass2;
    subpass2.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
    subpass2.colorAttachmentCount = 1;
    subpass2.pColorAttachments = &colorAttachmentRef;
    subpass2.pDepthStencilAttachment = &depthAttachmentRef;

    std::array<vk::SubpassDescription, 2> subpasses = { { subpass, subpass2 } };

    vk::SubpassDependency dependency;
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;

    vk::SubpassDependency dependency2;
    dependency2.srcSubpass = 0;
    dependency2.dstSubpass = 1;
    dependency2.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency2.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
    dependency2.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;

    std::array<vk::SubpassDependency, 2> dependencies = { { dependency, dependency2 } };

    vk::RenderPassCreateInfo renderPassInfo;
    renderPassInfo.attachmentCount = attachments.size();
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = subpasses.size();
    renderPassInfo.pSubpasses = subpasses.data();
    renderPassInfo.dependencyCount = dependencies.size();
    renderPassInfo.pDependencies = dependencies.data();

    _device->lock();
    vk::Result res = _device->getDevice().createRenderPass(&renderPassInfo, nullptr, &_renderPass);
    _device->unlock();
    if (res != vk::Result::eSuccess) {
        std::cerr << "Failed to create render pass! error:" << res << std::endl;
        std::abort();
    }
}

void Application::createGraphicsPipeline()
{
    std::cerr << "Creating graphics pipeline..." << std::endl;

    // Copy shaders directory from repo or change working directory
    const auto vertShaderCode = readFile("shaders/graphics/vert.spv");
    const auto fragShaderCode = readFile("shaders/graphics/frag.spv");
    _device->lock();
    std::cerr << "Creating vertex shader..." << std::endl;
    createShaderModule(_device->getDevice(), vertShaderCode, _vertShaderModule);
    std::cerr << "Creating fragment shader..." << std::endl;
    createShaderModule(_device->getDevice(), fragShaderCode, _fragShaderModule);
    std::cerr << "Shaders created!" << std::endl;

    vk::PipelineShaderStageCreateInfo vertShaderStageInfo;
    vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
    vertShaderStageInfo.module = _vertShaderModule;
    vertShaderStageInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo fragShaderStageInfo;
    fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
    fragShaderStageInfo.module = _fragShaderModule;
    fragShaderStageInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

    auto bindingDescription = GraphicsVertex::getBindingDescription();
    auto attributeDescriptions = GraphicsVertex::getAttributeDescriptions();

    vk::PipelineVertexInputStateCreateInfo vertexInputInfo;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    vk::PipelineInputAssemblyStateCreateInfo inputAssembly(vk::PipelineInputAssemblyStateCreateFlags(), vk::PrimitiveTopology::eTriangleList, VK_FALSE);

    vk::Viewport viewport(0.0f, 0.0f, static_cast<float>(_swapChainExtent.width), static_cast<float>(_swapChainExtent.height), 0.0f, 1.0f);

    vk::Rect2D scissor(vk::Offset2D(0, 0), _swapChainExtent);

    vk::PipelineViewportStateCreateInfo viewportState;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    vk::PipelineTessellationStateCreateInfo tesselationState;

    vk::PipelineRasterizationStateCreateInfo rasterizer;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;

    vk::PipelineMultisampleStateCreateInfo multisampling;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;

    vk::PipelineDepthStencilStateCreateInfo depthStencil(vk::PipelineDepthStencilStateCreateFlags(), VK_TRUE, VK_TRUE, vk::CompareOp::eLess, VK_FALSE, VK_FALSE);

    vk::PipelineColorBlendAttachmentState colorBlendAttachment;
    colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    colorBlendAttachment.blendEnable = VK_FALSE;

    vk::PipelineColorBlendStateCreateInfo colorBlending(vk::PipelineColorBlendStateCreateFlags(), VK_FALSE, vk::LogicOp::eCopy, 1, &colorBlendAttachment, { { 0.0f, 0.0f, 0.0f, 0.0f } });

    vk::DescriptorSetLayout setLayouts[] = { _graphicsDescriptorSetLayout };
    vk::PipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = setLayouts;

    vk::Result pipelineResult = _device->getDevice().createPipelineLayout(&pipelineLayoutInfo, nullptr, &_graphicsPipelineLayout);
    if (pipelineResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create pipeline layout! error:" << pipelineResult << std::endl;
        std::abort();
    }
    std::cerr << "Pipeline layout created!" << std::endl;

    vk::GraphicsPipelineCreateInfo pipelineInfo(vk::PipelineCreateFlags(), 2, shaderStages, &vertexInputInfo, &inputAssembly, &tesselationState, &viewportState, &rasterizer, &multisampling, &depthStencil, &colorBlending, nullptr, _graphicsPipelineLayout, _renderPass, 0, vk::Pipeline(), 0);

    std::cerr << "Creating pipeline cache..." << std::endl;
    vk::PipelineCacheCreateInfo cacheCreateInfo;
    vk::Result cacheResult = _device->getDevice().createPipelineCache(&cacheCreateInfo, nullptr, &_grapicsCache);
    if (cacheResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create pipeline cache! error:" << cacheResult << std::endl;
        std::abort();
    }

    vk::Result graphicsResult = _device->getDevice().createGraphicsPipelines(_grapicsCache, 1, &pipelineInfo, nullptr, &_graphicsPipeline);
    if (graphicsResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create graphics pipeline! error:" << graphicsResult << std::endl;
        std::abort();
    }
    _device->unlock();
    std::cerr << "Graphics pipeline created!" << std::endl;
}

void Application::createUiPipeline()
{
    std::cerr << "Creating UI pipeline..." << std::endl;

    // Copy shaders directory from repo or change working directory
    const auto vertShaderCode = readFile("shaders/ui/vert.spv");
    const auto fragShaderCode = readFile("shaders/ui/frag.spv");
    _device->lock();
    std::cerr << "Creating vertex shader..." << std::endl;
    createShaderModule(_device->getDevice(), vertShaderCode, _uivertShaderModule);
    std::cerr << "Creating fragment shader..." << std::endl;
    createShaderModule(_device->getDevice(), fragShaderCode, _uifragShaderModule);
    std::cerr << "Shaders created!" << std::endl;

    vk::PipelineShaderStageCreateInfo vertShaderStageInfo;
    vertShaderStageInfo.stage = vk::ShaderStageFlagBits::eVertex;
    vertShaderStageInfo.module = _uivertShaderModule;
    vertShaderStageInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo fragShaderStageInfo;
    fragShaderStageInfo.stage = vk::ShaderStageFlagBits::eFragment;
    fragShaderStageInfo.module = _uifragShaderModule;
    fragShaderStageInfo.pName = "main";

    vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

    auto bindingDescription = GraphicsVertex::getBindingDescription();
    auto attributeDescriptions = GraphicsVertex::getAttributeDescriptions();

    vk::PipelineVertexInputStateCreateInfo vertexInputInfo;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.vertexAttributeDescriptionCount = attributeDescriptions.size();
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

    vk::PipelineInputAssemblyStateCreateInfo inputAssembly(vk::PipelineInputAssemblyStateCreateFlags(), vk::PrimitiveTopology::eTriangleList, VK_FALSE);

    vk::Viewport viewport(0.0f, 0.0f, static_cast<float>(_swapChainExtent.width), static_cast<float>(_swapChainExtent.height), 0.0f, 1.0f);

    vk::Rect2D scissor(vk::Offset2D(0, 0), _swapChainExtent);

    vk::PipelineViewportStateCreateInfo viewportState;
    viewportState.viewportCount = 1;
    viewportState.pViewports = &viewport;
    viewportState.scissorCount = 1;
    viewportState.pScissors = &scissor;

    vk::PipelineTessellationStateCreateInfo tesselationState;

    vk::PipelineRasterizationStateCreateInfo rasterizer;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = vk::PolygonMode::eFill;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = vk::CullModeFlagBits::eBack;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;

    vk::PipelineMultisampleStateCreateInfo multisampling;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;

    vk::PipelineDepthStencilStateCreateInfo depthStencil(vk::PipelineDepthStencilStateCreateFlags(), VK_TRUE, VK_TRUE, vk::CompareOp::eLess, VK_FALSE, VK_FALSE);

    vk::PipelineColorBlendAttachmentState colorBlendAttachment;
    colorBlendAttachment.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    colorBlendAttachment.blendEnable = VK_FALSE;

    vk::PipelineColorBlendStateCreateInfo colorBlending(vk::PipelineColorBlendStateCreateFlags(), VK_FALSE, vk::LogicOp::eCopy, 1, &colorBlendAttachment, { { 0.0f, 0.0f, 0.0f, 0.0f } });

    vk::DescriptorSetLayout setLayouts[] = { _uiDescriptorSetLayout };
    vk::PipelineLayoutCreateInfo pipelineLayoutInfo = {};
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = setLayouts;

    vk::Result pipelineResult = _device->getDevice().createPipelineLayout(&pipelineLayoutInfo, nullptr, &_uiPipelineLayout);
    if (pipelineResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create pipeline layout! error:" << pipelineResult << std::endl;
        std::abort();
    }
    std::cerr << "Pipeline layout created!" << std::endl;

    vk::GraphicsPipelineCreateInfo pipelineInfo(vk::PipelineCreateFlags(), 2, shaderStages, &vertexInputInfo, &inputAssembly, &tesselationState, &viewportState, &rasterizer, &multisampling, &depthStencil, &colorBlending, nullptr, _uiPipelineLayout, _renderPass, 1, vk::Pipeline(), 0);

    std::cerr << "Creating pipeline cache..." << std::endl;
    vk::PipelineCacheCreateInfo cacheCreateInfo;
    vk::Result cacheResult = _device->getDevice().createPipelineCache(&cacheCreateInfo, nullptr, &_uiCache);
    if (cacheResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create pipeline cache! error:" << cacheResult << std::endl;
        std::abort();
    }

    vk::Result graphicsResult = _device->getDevice().createGraphicsPipelines(_uiCache, 1, &pipelineInfo, nullptr, &_uiPipeline);
    if (graphicsResult != vk::Result::eSuccess) {
        std::cerr << "Failed to create graphics pipeline! error:" << graphicsResult << std::endl;
        std::abort();
    }
    _device->unlock();
    std::cerr << "Graphics pipeline created!" << std::endl;
}

void Application::createFramebuffers()
{
    std::cerr << "Creating framebuffers..." << std::endl;
    _swapChainFramebuffers.resize(_swapChainImageViews.size());
    size_t size = _swapChainImageViews.size();
    for (size_t i = 0; i < size; i++) {
        std::array<vk::ImageView, 2> attachments = { { _swapChainImageViews[i], _depthImageView } };

        vk::FramebufferCreateInfo framebufferInfo;
        framebufferInfo.renderPass = _renderPass;
        framebufferInfo.attachmentCount = attachments.size();
        framebufferInfo.pAttachments = attachments.data();
        framebufferInfo.width = _swapChainExtent.width;
        framebufferInfo.height = _swapChainExtent.height;
        framebufferInfo.layers = 1;

        _device->lock();
        vk::Result res = _device->getDevice().createFramebuffer(&framebufferInfo, nullptr, &_swapChainFramebuffers[i]);
        _device->unlock();
        if (res != vk::Result::eSuccess) {
            std::cerr << "Failed to create framebuffer! error:" << res << std::endl;
            std::abort();
        }
    }
}

void Application::createCommandPool()
{
    std::cerr << "Creating command pools..." << std::endl;
    vk::CommandPoolCreateInfo poolInfo;
    poolInfo.queueFamilyIndex = static_cast<uint32_t>(_queueFamilyIndices.graphicsFamily);
    _device->lock();
    vk::CommandPoolCreateInfo memoryPoolInfo;
    poolInfo.queueFamilyIndex = static_cast<uint32_t>(_queueFamilyIndices.graphicsFamily);
    vk::Result memoryRes = _device->getDevice().createCommandPool(&memoryPoolInfo, nullptr, &_memoryCommandPool);
    if (memoryRes != vk::Result::eSuccess) {
        std::cerr << "Failed to create memory command pool! error:" << memoryRes << std::endl;
        std::abort();
    }
    _device->unlock();
}

void Application::createSemaphores()
{
    std::cerr << "Creating semaphores..." << std::endl;
    vk::SemaphoreCreateInfo semaphoreInfo;
    _device->lock();
    if (_device->getDevice().createSemaphore(&semaphoreInfo, nullptr, &_imageAvailableSemaphore) != vk::Result::eSuccess || _device->getDevice().createSemaphore(&semaphoreInfo, nullptr, &_renderFinishedSemaphore) != vk::Result::eSuccess) {
        std::cerr << "Failed to create semaphores!" << std::endl;
        std::abort();
    }

    vk::FenceCreateInfo fenceCreateInfo = {};
    fenceCreateInfo.flags = vk::FenceCreateFlagBits::eSignaled;
    _waitFences.resize(_swapChainFramebuffers.size());
    for (auto& fence : _waitFences) {
        if (_device->getDevice().createFence(&fenceCreateInfo, nullptr, &fence) != vk::Result::eSuccess) {
            std::cerr << "Failed to create fence!" << std::endl;
            std::abort();
        }
    }
    _device->unlock();
}

void Application::updateUniformBuffer()
{
    const MVPBufferObject& ubo = _camera.getMVP();
    _uniformObject.updateData(reinterpret_cast<const void*>(&ubo), sizeof(MVPBufferObject), _memoryManager);
}

void Application::drawFrame()
{
    uint32_t imageIndex;
    _device->lock();
    vk::Result result = _device->getDevice().acquireNextImageKHR(_swapChain, std::numeric_limits<uint64_t>::max(), _imageAvailableSemaphore, nullptr, &imageIndex);
    _device->unlock();
    if (result == vk::Result::eErrorOutOfDateKHR) {
        recreateSwapChain();
        return;
    } else if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR) {
        std::cerr << "Failed to acquire swap chain image! error:" << result << std::endl;
        std::abort();
    }

    _device->lock();
    _device->getDevice().waitForFences(1, &_waitFences[imageIndex], VK_TRUE, std::numeric_limits<uint64_t>::max());
    _device->unlock();
    cleanupOldRender();
    setRenderData(_entities);
    vk::RenderPassBeginInfo renderPassInfo;
    renderPassInfo.renderPass = _renderPass;
    renderPassInfo.framebuffer = _swapChainFramebuffers[imageIndex];
    renderPassInfo.renderArea.offset = vk::Offset2D(0, 0);
    renderPassInfo.renderArea.extent = _swapChainExtent;
    std::array<vk::ClearValue, 2> clearValues = {};
    clearValues[0].color = vk::ClearColorValue(std::array<float, 4>{ { 0.1f, 0.2f, 0.1f, 1.0f } });
    clearValues[1].depthStencil = vk::ClearDepthStencilValue(1.0f, 0);

    renderPassInfo.clearValueCount = clearValues.size();
    renderPassInfo.pClearValues = clearValues.data();
    doRender(renderPassInfo);
    waitRender();
    const std::vector<vk::CommandBuffer>& currentBuffers = getRenderResults();
    _device->lock();
    _device->getDevice().resetFences(1, &_waitFences[imageIndex]);
    _device->unlock();

    vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

    vk::SubmitInfo submitInfo;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &_imageAvailableSemaphore;
    submitInfo.pWaitDstStageMask = waitStages;
    submitInfo.commandBufferCount = currentBuffers.size();
    submitInfo.pCommandBuffers = currentBuffers.data();
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &_renderFinishedSemaphore;

    vk::Result submitResult = _graphicsQueue.submit(1, &submitInfo, _waitFences[imageIndex]);
    if (submitResult != vk::Result::eSuccess) {
        std::cerr << "failed to submit draw command buffer! error:" << submitResult << std::endl;
        std::abort();
    }

    vk::PresentInfoKHR presentInfo;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &_renderFinishedSemaphore;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &_swapChain;
    presentInfo.pImageIndices = &imageIndex;

    vk::Result presentResult = _presentQueue.presentKHR(&presentInfo);
    if (presentResult == vk::Result::eErrorOutOfDateKHR || presentResult == vk::Result::eSuboptimalKHR) {
        recreateSwapChain();
    } else if (presentResult != vk::Result::eSuccess) {
        std::cerr << "failed to present swap chain image! error:" << presentResult << std::endl;
        std::abort();
    }
}

void Application::recreateSwapChain()
{
    std::cerr << "Recreating swap chain..." << std::endl;
    _graphicsQueue.waitIdle();
    _presentQueue.waitIdle();
    _device->lock();
    _device->getDevice().waitIdle();
    for (const vk::ImageView& imageview : _swapChainImageViews) {
        _device->getDevice().destroyImageView(imageview);
    }
    for (const vk::Framebuffer& framebuffer : _swapChainFramebuffers) {
        _device->getDevice().destroyFramebuffer(framebuffer);
    }
    _device->unlock();
    _renderingThread.destroyHandlers();
    _device->lock();
    _device->getDevice().destroyImage(_depthImage);
    _device->getDevice().freeMemory(_depthImageMemory);
    _device->getDevice().destroyImageView(_depthImageView);
    _device->getDevice().destroyShaderModule(_vertShaderModule);
    _device->getDevice().destroyShaderModule(_fragShaderModule);
    _device->getDevice().destroyPipelineCache(_grapicsCache);
    _device->getDevice().destroyPipelineCache(_uiCache);
    _device->getDevice().destroyPipelineLayout(_graphicsPipelineLayout);
    _device->getDevice().destroyPipeline(_graphicsPipeline);
    _device->getDevice().destroyRenderPass(_renderPass);
    _device->getDevice().destroySwapchainKHR(_swapChain);
    _device->unlock();
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createDepthResources();
    createFramebuffers();
    initRenderingThread();
}

void Application::loadDrawableObjects()
{
    for (auto& entity : _entities) {
        entity.setMvpBuffer(_uniformObject.buffer());
        entity.loadData(_memoryManager);
    }
}

void Application::createGraphicsDescriptorSetLayout()
{
    vk::DescriptorSetLayoutBinding uboLayoutBinding;
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
    uboLayoutBinding.pImmutableSamplers = nullptr;
    uboLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;

    vk::DescriptorSetLayoutBinding objectTransformLayoutBinding;
    objectTransformLayoutBinding.binding = 1;
    objectTransformLayoutBinding.descriptorCount = 1;
    objectTransformLayoutBinding.descriptorType = vk::DescriptorType::eUniformBuffer;
    objectTransformLayoutBinding.pImmutableSamplers = nullptr;
    objectTransformLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eVertex;

    vk::DescriptorSetLayoutBinding textureSamplerLayoutBinding;
    textureSamplerLayoutBinding.binding = 2;
    textureSamplerLayoutBinding.descriptorCount = 1;
    textureSamplerLayoutBinding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    textureSamplerLayoutBinding.pImmutableSamplers = nullptr;
    textureSamplerLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;

    vk::DescriptorSetLayoutBinding normalSamplerLayoutBinding;
    normalSamplerLayoutBinding.binding = 3;
    normalSamplerLayoutBinding.descriptorCount = 1;
    normalSamplerLayoutBinding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    normalSamplerLayoutBinding.pImmutableSamplers = nullptr;
    normalSamplerLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;

    std::array<vk::DescriptorSetLayoutBinding, 4> bindings = { { uboLayoutBinding,
        objectTransformLayoutBinding, textureSamplerLayoutBinding, normalSamplerLayoutBinding } };

    vk::DescriptorSetLayoutCreateInfo layoutInfo;
    layoutInfo.bindingCount = bindings.size();
    layoutInfo.pBindings = bindings.data();
    _device->lock();
    if (_device->getDevice().createDescriptorSetLayout(&layoutInfo, nullptr, &_graphicsDescriptorSetLayout) != vk::Result::eSuccess) {
        std::cerr << "Failed to create descriptor set layout!" << std::endl;
        std::abort();
    }
    _device->unlock();
}

void Application::createUiDescriptorSetLayout()
{
    vk::DescriptorSetLayoutBinding textureSamplerLayoutBinding;
    textureSamplerLayoutBinding.binding = 0;
    textureSamplerLayoutBinding.descriptorCount = 1;
    textureSamplerLayoutBinding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
    textureSamplerLayoutBinding.pImmutableSamplers = nullptr;
    textureSamplerLayoutBinding.stageFlags = vk::ShaderStageFlagBits::eFragment;

    std::array<vk::DescriptorSetLayoutBinding, 1> bindings = { { textureSamplerLayoutBinding } };

    vk::DescriptorSetLayoutCreateInfo layoutInfo;
    layoutInfo.bindingCount = bindings.size();
    layoutInfo.pBindings = bindings.data();
    _device->lock();
    if (_device->getDevice().createDescriptorSetLayout(&layoutInfo, nullptr, &_uiDescriptorSetLayout) != vk::Result::eSuccess) {
        std::cerr << "Failed to create descriptor set layout!" << std::endl;
        std::abort();
    }
    _device->unlock();
}

void Application::createDepthResources()
{
    vk::Format depthFormat = findDepthFormat(_physicalDevice);
    std::cerr << "Swapchain width: " << _swapChainExtent.width << " height: " << _swapChainExtent.height << std::endl;
    _camera.setupCamera(_swapChainExtent.width, _swapChainExtent.height);
    _memoryManager.createImage(_swapChainExtent.width, _swapChainExtent.height, depthFormat, vk::ImageTiling::eOptimal, vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal, _depthImage, _depthImageMemory);
    _device->lock();
    MemoryManager::createImageView(_device->getDevice(), _depthImage, depthFormat, vk::ImageAspectFlagBits::eDepth, _depthImageView);
    _device->unlock();
    _memoryManager.transitionImageLayout(_depthImage, depthFormat, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal);
}

void Application::cleanupOldRender()
{
    _renderingThread.doCleanup();
}

void Application::doRender(const vk::RenderPassBeginInfo& renderPassInfo)
{
    _renderingThread.doRender(renderPassInfo);
}

void Application::setRenderData(const std::vector<DrawableObject>& entities)
{
    _renderingThread.setRenderingData(entities);
}

void Application::waitRender()
{
    _renderingThread.waitForFinished();
}
std::vector<vk::CommandBuffer> Application::getRenderResults()
{
    return _renderingThread.getResults();
}

void Application::createUniformBuffer()
{
    const MVPBufferObject& ubo = _camera.getMVP();
    size_t bufferSize = sizeof(MVPBufferObject);
    _uniformObject.loadToDevice(reinterpret_cast<const void*>(&ubo), bufferSize, vk::BufferUsageFlagBits::eUniformBuffer, _memoryManager);
}

void Application::createDescriptorPool()
{
    std::cerr << "Creating descriptor pool..." << std::endl;
    std::array<vk::DescriptorPoolSize, 4> poolSizes;
    poolSizes[0].type = vk::DescriptorType::eUniformBuffer;
    poolSizes[0].descriptorCount = 1;
    poolSizes[1].type = vk::DescriptorType::eUniformBuffer;
    poolSizes[1].descriptorCount = 1;
    poolSizes[2].type = vk::DescriptorType::eCombinedImageSampler;
    poolSizes[2].descriptorCount = 1;
    poolSizes[3].type = vk::DescriptorType::eCombinedImageSampler;
    poolSizes[3].descriptorCount = 1;

    vk::DescriptorPoolCreateInfo poolInfo(vk::DescriptorPoolCreateFlags(), 2000, poolSizes.size(), poolSizes.data());
    _device->lock();
    if (_device->getDevice().createDescriptorPool(&poolInfo, nullptr, &_descriptorPool) != vk::Result::eSuccess) {
        std::cerr << "Failed to create descriptor pool!" << std::endl;
        std::abort();
    }
    _device->unlock();
}
