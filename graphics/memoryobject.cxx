#include "memoryobject.hpp"
#include "memorymanager.hpp"

vk::Buffer MemoryObject::buffer() const
{
    return _buffer;
}

vk::DeviceMemory MemoryObject::bufferMemory() const
{
    return _bufferMemory;
}

bool MemoryObject::keepStagingBuffer() const
{
    return _keepStagingBuffer;
}

void MemoryObject::loadToDevice(const void* data, size_t size, const vk::BufferUsageFlags& usage, const MemoryManager& manager)
{
    manager.loadBuffer(data, size, vk::BufferUsageFlagBits::eTransferDst | usage, _buffer, _bufferMemory, _stagingBuffer, _stagingBufferMemory, _keepStagingBuffer);
}

void MemoryObject::freeMemory(vk::Device& device)
{
    device.destroyBuffer(_buffer);
    device.freeMemory(_bufferMemory);
    if (_keepStagingBuffer) {
        device.destroyBuffer(_stagingBuffer);
        device.freeMemory(_stagingBufferMemory);
    }
}

void MemoryObject::updateData(const void* data, size_t size, const MemoryManager& manager)
{
    if (keepStagingBuffer()) {
        manager.copyToStagingBuffer(data, size, _stagingBufferMemory);
        manager.copyBuffer(_stagingBuffer, _buffer, size);
    } else {
        std::cerr << "MemoryObject staging buffer missing!" << std::endl;
        std::abort();
    }
}

vk::Buffer MemoryObject::stagingBuffer() const
{
    return _stagingBuffer;
}

vk::DeviceMemory MemoryObject::stagingBufferMemory() const
{
    return _stagingBufferMemory;
}

MemoryObject::MemoryObject(bool keepStagingBuffer)
    : _keepStagingBuffer(keepStagingBuffer)
{
}

MemoryObject::~MemoryObject()
{
}
