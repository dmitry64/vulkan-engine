#ifndef APPLICATION_INCONCE_H
#define APPLICATION_INCONCE_H

#include <vector>
#include <vulkan/vulkan.hpp>

#include "assetloader.hpp"
#include "camera.hpp"
#include "debugcallbacks.hpp"
#include "devicewrapper.hpp"
#include "drawableobject.hpp"
#include "graphicsvertex.hpp"
#include "helperfunctions.hpp"
#include "mainthread.hpp"
#include "memorymanager.hpp"
#include "memoryobject.hpp"
#include "mvpbufferobject.hpp"
#include "renderthreadpool.hpp"
#include "window.hpp"
#include <memory>
#include <mutex>

class Application {
public:
    Application();
    ~Application();
    void run();

private:
    Window _window;
    std::shared_ptr<DeviceWrapper> _device;
    std::shared_ptr<MainThread> _mainThread;
    MemoryManager _memoryManager;
    vk::Instance _instance;
    vk::SurfaceKHR _surface;
    VkDebugReportCallbackEXT _callback;
    vk::PhysicalDevice _physicalDevice;
    vk::Queue _graphicsQueue;
    vk::Queue _presentQueue;
    vk::PipelineCache _grapicsCache;
    vk::PipelineCache _uiCache;

    QueueFamilyIndices _queueFamilyIndices;

    vk::SwapchainKHR _swapChain;
    std::vector<vk::Image> _swapChainImages;
    vk::Format _swapChainImageFormat;
    vk::Extent2D _swapChainExtent;
    std::vector<vk::ImageView> _swapChainImageViews;
    std::vector<vk::Framebuffer> _swapChainFramebuffers;

    vk::RenderPass _renderPass;
    vk::DescriptorSetLayout _graphicsDescriptorSetLayout;
    vk::DescriptorSetLayout _uiDescriptorSetLayout;
    vk::PipelineLayout _graphicsPipelineLayout;
    vk::PipelineLayout _uiPipelineLayout;
    vk::Pipeline _graphicsPipeline;
    vk::Pipeline _uiPipeline;

    vk::CommandPool _memoryCommandPool;

    MemoryObject _uniformObject;

    vk::DescriptorPool _descriptorPool;
    vk::DescriptorSet _descriptorSet;

    vk::Image _depthImage;
    vk::DeviceMemory _depthImageMemory;
    vk::ImageView _depthImageView;

    vk::Semaphore _imageAvailableSemaphore;
    vk::Semaphore _renderFinishedSemaphore;
    std::vector<vk::Fence> _waitFences;

    vk::ShaderModule _vertShaderModule;
    vk::ShaderModule _fragShaderModule;

    vk::ShaderModule _uivertShaderModule;
    vk::ShaderModule _uifragShaderModule;

    AssetLoader _assetLoader;
    std::vector<DrawableObject> _entities;
    Camera _camera;
    RenderThreadPool _renderingThread;

private:
    void initVulkan();
    void mainLoop();
    void startMainThread();
    void stopMainThread();
    void initRenderingThread();
    void destroyVulkan();
    void createInstance();
    void setupDebugCallback();
    void createSurface();
    void pickPhysicalDevice();
    void createLogicalDevice();
    void createMemoryManager();
    void createSwapChain();
    void createImageViews();
    void createRenderPass();
    void createGraphicsPipeline();
    void createUiPipeline();
    void createFramebuffers();
    void createCommandPool();
    void createSemaphores();
    void drawFrame();
    void recreateSwapChain();
    void loadDrawableObjects();
    void createUniformBuffer();
    void createDescriptorPool();
    void updateUniformBuffer();
    void createGraphicsDescriptorSetLayout();
    void createUiDescriptorSetLayout();
    void createDepthResources();
    void cleanupOldRender();
    void doRender(const vk::RenderPassBeginInfo& renderPassInfo);
    void setRenderData(const std::vector<DrawableObject>& entities);
    void waitRender();
    std::vector<vk::CommandBuffer> getRenderResults();
};

#endif // APPLICATION_H
