#ifndef MAINTHREAD_HPP
#define MAINTHREAD_HPP

#include <atomic>
#include <thread>

class MainThread {
    std::atomic_bool _runSwitch;
    std::thread _thread;

private:
    void tick(long deltaTime);
    void init();
    void destroy();
    void handleInput();

public:
    MainThread();
    void run();
    void stop();
};

#endif // MAINTHREAD_HPP
