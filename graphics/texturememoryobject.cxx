#include "texturememoryobject.hpp"

std::vector<unsigned char> TextureMemoryObject::imageData() const
{
    return _imageData;
}

void TextureMemoryObject::setImageData(const std::vector<unsigned char>& imageData)
{
    _imageData = imageData;
}

void TextureMemoryObject::loadData(MemoryManager& manager)
{
    manager.createTextureImage(reinterpret_cast<const void*>(_imageData.data()), _width, _height, vk::Format::eR8G8B8A8Unorm, _textureImage, _textureImageMemory, _textureImageStagingBuffer, _textureImageMemoryStaging);
    manager.createTextureImageView(_textureImage, vk::Format::eR8G8B8A8Unorm, _textureImageView);
    manager.createTextureSampler(_textureSampler);
}

void TextureMemoryObject::freeMemory(vk::Device& device)
{
    device.destroySampler(_textureSampler);
    device.destroyImageView(_textureImageView);
    device.destroyImage(_textureImage);
    _imageData.clear();
    _height = 0;
    _width = 0;
}

uint32_t TextureMemoryObject::height() const
{
    return _height;
}

void TextureMemoryObject::setHeight(const uint32_t& height)
{
    _height = height;
}

uint32_t TextureMemoryObject::width() const
{
    return _width;
}

void TextureMemoryObject::setWidth(const uint32_t& width)
{
    _width = width;
}

vk::ImageView TextureMemoryObject::textureImageView() const
{
    return _textureImageView;
}

vk::Sampler TextureMemoryObject::textureSampler() const
{
    return _textureSampler;
}

void TextureMemoryObject::resetImageToDummy()
{
    uint32_t width = 32;
    uint32_t height = 32;
    std::vector<unsigned char> textute(4 * width * height);
    for (uint32_t i = 0; i < width; ++i) {
        for (uint32_t j = 0; j < height; ++j) {
            textute[(i * width + j) * 4] = ((i * j) % 2) ? 0xFF : 0x00;
            textute[(i * width + j) * 4 + 1] = 0x00;
            textute[(i * width + j) * 4 + 2] = 0x00;
            textute[(i * width + j) * 4 + 3] = 0xFF;
        }
    }
    setImageData(textute);
    setHeight(width);
    setWidth(height);
}

TextureMemoryObject::TextureMemoryObject()
    : _height(0)
    , _width(0)

{
}
