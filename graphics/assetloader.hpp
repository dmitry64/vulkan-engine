#ifndef ASSETLOADER_H
#define ASSETLOADER_H

#include "drawableobject.hpp"
#include "graphicsvertex.hpp"
#include "texturememoryobject.hpp"
#include <vector>

namespace tinygltf {
class Model;
}

class AssetLoader {
public:
    AssetLoader();
    std::vector<DrawableObject> loadAssets(const std::string& filePath);

private:
    void copyPositionVertices(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts);
    void copyColors(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts);
    void copyNormals(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts);
    void copyTexCoords(std::vector<float>& floats, std::vector<GraphicsVertex>& assetVerts);
    void handleNode(int nodeId, tinygltf::Model& model, DrawableObject& parent, const std::vector<TextureMemoryObject>& textures);
};

#endif // ASSETLOADER_H
