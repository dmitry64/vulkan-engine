#ifndef TEXTUREMEMORYOBJECT_H
#define TEXTUREMEMORYOBJECT_H

#include "memorymanager.hpp"
#include <vulkan/vulkan.hpp>

class TextureMemoryObject {
    vk::Image _textureImage;
    vk::DeviceMemory _textureImageMemory;
    vk::Buffer _textureImageStagingBuffer;
    vk::DeviceMemory _textureImageMemoryStaging;
    vk::ImageView _textureImageView;
    vk::Sampler _textureSampler;

    std::vector<unsigned char> _imageData;
    uint32_t _height;
    uint32_t _width;

public:
    TextureMemoryObject();
    std::vector<unsigned char> imageData() const;
    void setImageData(const std::vector<unsigned char>& imageData);
    void loadData(MemoryManager& manager);
    void freeMemory(vk::Device& device);
    uint32_t height() const;
    void setHeight(const uint32_t& height);
    uint32_t width() const;
    void setWidth(const uint32_t& width);
    vk::ImageView textureImageView() const;
    vk::Sampler textureSampler() const;
    void resetImageToDummy();
};

#endif // TEXTUREMEMORYOBJECT_H
