#include "camera.hpp"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include "inputhandler.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

Camera::Camera()
{
    _eye = glm::vec3(0.0f, 0.0f, 4.0f);
    _target = glm::vec3(0.0f, 0.0f, 0.0f);
    _up = glm::vec3(0.0f, 1.0f, 0.0f);
    _prevMousePos = glm::vec2(0.5f, 0.5f);
    _width = 0;
    _height = 0;
    _horizontalAngle = 3.14f;
    _verticalAngle = 0.0f;
}

void Camera::setupCamera(uint32_t width, uint32_t height)
{
    _width = width;
    _height = height;
    float ratio = static_cast<float>(width) / static_cast<float>(height);
    _proj = glm::perspective(glm::radians(45.0f), ratio, 0.1f, 300.0f);
    _lastTime = std::chrono::high_resolution_clock::now();
}

MVPBufferObject Camera::getMVP()
{
    auto currentTime = std::chrono::high_resolution_clock::now();
    //auto currentTime = std::chrono::high_resolution_clock::now();
    float deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - _lastTime).count() / 1000.0f;
    _lastTime = currentTime;

    glm::vec2 mousePos = InputHandler::Instance().mousePos();

    glm::vec2 delta = mousePos - _prevMousePos;

    //std::cerr << "DELTA:" << delta.x << " " << delta.y << std::endl;

    float speed = 3.0f; // 3 units / second
    float mouseSpeed = 0.005f;
    _horizontalAngle += mouseSpeed * deltaTime * float(_width / 2 - mousePos.x);
    _verticalAngle += mouseSpeed * deltaTime * float(_height / 2 - mousePos.y);
    glm::vec3 direction(
        cos(_verticalAngle) * sin(_horizontalAngle),
        sin(_verticalAngle),
        cos(_verticalAngle) * cos(_horizontalAngle));

    direction = glm::normalize(direction);

    glm::vec3 right = glm::vec3(
        sin(_horizontalAngle - 3.14f / 2.0f),
        0,
        cos(_horizontalAngle - 3.14f / 2.0f));
    glm::vec3 up = glm::cross(right, direction);

    //std::cerr << "ANGLE: " << _horizontalAngle << " " << _verticalAngle << std::endl;
    _prevMousePos = mousePos;
    MVPBufferObject ubo;
    ubo.model = glm::mat4(1.0f); //glm::rotate(glm::mat4(1.0f),  * glm::radians(90.0f) / 1.0f, glm::vec3(0.0f, 1.0f, 0.0f));
    ubo.view = glm::lookAt(_eye, _eye + direction, up);
    ubo.proj = _proj;
    ubo.proj[1][1] *= -1.0f;
    return ubo;
}
